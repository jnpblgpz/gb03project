package es.unex.giiis.asee.transfugaz;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * Tests funcionales para el caso de uso CU04 - Añadir usuario
 * <p>
 * Comprueba que como Admin se pueda crear un nuevo usuario, por ejemplo, otro administrador
 */
@LargeTest
public class CU04UITests {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityRule =
            new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";
    // Datos del nuevo usuario
    private final String nuevoUsuario = "nuevoAdmin";
    private final String nuevoUsuarioClave = "1234";

    @Test
    public void shouldRegisterNewAdminUser() {

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.usuariosFragment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_inferior),
                                        0),
                                2),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        // ASEVERAR QUE EL BOTÓN PARA AÑADIR USUARIO EXISTE PARA EL ADMINISTRADOR
        onView(withId(R.id.acc_añadir_usuario)).check(matches(isCompletelyDisplayed()));

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.acc_añadir_usuario),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.et_nombreUsuario),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText4.perform(replaceText(nuevoUsuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.et_nombreUsuario), withText(nuevoUsuario),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText5.perform(pressImeActionButton());

        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.tiet_pwd),
                        isDisplayed()));
        textInputEditText.perform(replaceText(nuevoUsuarioClave), closeSoftKeyboard());
        textInputEditText.perform(pressImeActionButton());

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.acc_confirmar_edicion_usuario),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView2.perform(click());

        // ASERVERAR QUE EL NUEVO USUARIO EXISTE Y EL NOMBRE EN LA LISTA DE USUARIOS QUE SE MUESTRA ES EL CORRECTO
        onView(withText(nuevoUsuario)).check(matches(isCompletelyDisplayed()));

        // ASEVERAR QUE EL NUEVO USUARIO TIENE EL ROL ADMIN
        onView(allOf(withId(R.id.rolUsuario),
                withParent(withParent(withId(R.id.rv_usuarios))),
                withParent(hasDescendant(withText(nuevoUsuario))),
                isDisplayed()))
                .check(matches(withText(Rol.ADMIN.toString())));
    }

    @After
    public void tearDown() {
        // Eliminar el usuario de prueba que se debe haber añadido
        UsuarioDAO daoUsuario =
                TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoUsuario();
        daoUsuario.borrar(nuevoUsuario);
    }
}
