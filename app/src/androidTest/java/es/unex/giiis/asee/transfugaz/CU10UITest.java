package es.unex.giiis.asee.transfugaz;


import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Calendar;

import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

/**
 * Pruebas funcionales para el CU10 - Listar y filtrar viajes
 *
 * Comprueba que es posible filtrar los viajes por conductor y que el filtro se puede reestablecer
 */
@LargeTest
public class CU10UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule =
            new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";

    // Identificadores de los viajes de prueba
    private final String idViajeTca = "SAUCAN13";
    private final String idViajeConductor = "ANARUA11";
    // Cuántos viajes hay en total
    private final int cuantosViajesTotal = 2;
    // Cuántos están asignados al conductor "conductor"
    private final int cuantosViajesConductor = 1;

    @Before
    public void setUp() throws Exception {
        Calendar fechaDescarga;
        TransFugazDB roomDB = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext());
        ViajeDAO daoViaje = roomDB.getDaoViaje();
        UsuarioDAO daoUsuario = roomDB.getDaoUsuario();

        Long idTca = daoUsuario.getPorNombreNLD("tca").getId();
        Long idConductor = daoUsuario.getPorNombreNLD("conductor").getId();

        // Viajes de prueba

        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1602579600);
        // Lo conduce el TCA
        Viaje viaje1 = new Viaje(idViajeTca, idTca, "Sauce 20, 28942 Fuenlabrada, Madrid",
                "Can Fenosa 12, 08107 Barcelona", fechaDescarga,
                Arrays.asList("Gigabyte B450M DS3H", "Zotac GAMING GeForce RTX2060 6GB GDDR6"),
                506.685f, false);

        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1605078000);
        // Lo conduce el conductor
        Viaje viaje2 = new Viaje(idViajeConductor, idConductor, "Ana Mariscal, 10004 Cáceres",
                "Rua Zapatería, 10600 Plasencia, Cáceres", fechaDescarga,
                Arrays.asList("Kingston A400 SSD 240GB", "MSI B450 Gaming Plus MAX"),
                67.93f, false);

        // Insertar los viajes de prueba

        daoViaje.insertar(viaje1);
        daoViaje.insertar(viaje2);
    }

    @Test
    public void shouldFilterJourneysByDriver() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.usuario), withText(usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction tabView = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.contenedor_pestañas),
                                0),
                        1),
                        isDisplayed()));
        tabView.perform(click());

        // ASEVERAR QUE ANTES DE APLICAR NINGÚN FILTRO SE MUESTREN TODOS LOS VIAJES
        onView(withId(R.id.rv_viajes)).check(matches(hasChildCount(cuantosViajesTotal)));

        // ASEVERAR QUE EN EFECTO SE MUESTRAN LOS IDENTIFICADORES CORRECTOS
        onView(withText(idViajeTca)).check(matches(isCompletelyDisplayed()));
        onView(withText(idViajeConductor)).check(matches(isCompletelyDisplayed()));

        // Filtrar por conductor "conductor"

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.acc_filtrar_viajes),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        DataInteraction appCompatTextView = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.FrameLayout")),
                                0)))
                .atPosition(0);
        appCompatTextView.perform(click());

        // ASEVERAR QUE AL APLICAR EL FILTRO DE VIAJES CUYO CONDUCTOR ES "conductor" MUESTRA 1 VIAJE
        onView(withId(R.id.rv_viajes)).check(matches(hasChildCount(cuantosViajesConductor)));

        // ASEVERAR QUE SE MUESTRA EL IDENTIFICADOR DEL VIAJE 2, ASIGNADO AL CONDUCTOR QUE FILTRA
        onView(withText(idViajeConductor)).check(matches(isCompletelyDisplayed()));

        // Restablecer el filtro

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.acc_filtrar_viajes),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView2.perform(click());

        DataInteraction appCompatTextView2 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.FrameLayout")),
                                0)))
                .atPosition(2);
        appCompatTextView2.perform(click());

        // ASEVERAR QUE SE PUEDE RESTABLECER EL FILTRO
        onView(withId(R.id.rv_viajes)).check(matches(hasChildCount(cuantosViajesTotal)));

        // ASEVERAR QUE EN EFECTO SE MUESTRAN LOS IDENTIFICADORES CORRECTOS DE NUEVO
        onView(withText(idViajeTca)).check(matches(isCompletelyDisplayed()));
        onView(withText(idViajeConductor)).check(matches(isCompletelyDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
        ViajeDAO daoViaje =
                TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoViaje();

        // Eliminar los viajes de prueba
        daoViaje.borrar(idViajeTca);
        daoViaje.borrar(idViajeConductor);
    }
}
