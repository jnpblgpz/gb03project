package es.unex.giiis.asee.transfugaz;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.is;

@LargeTest
public class CU12UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<>(MainActivity.class);

	private final String USUARIO = "admin";
	private final String CONTRASEÑA = "admin";
	private final String ID_VIAJE_PRUEBA = "TEST";

	@Before
	public void setUp() throws Exception {
		TransFugazDB roomDB = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext());
		ViajeDAO daoViaje = roomDB.getDaoViaje();
		Calendar fechaDescarga;

		// Viaje de prueba
		fechaDescarga = Calendar.getInstance();
		fechaDescarga.setTimeInMillis(1602579600);
		daoViaje.insertar(new Viaje(ID_VIAJE_PRUEBA, null, "origen", "destino", fechaDescarga,
			new ArrayList<String>(), 123, false));
	}

	@Test
	public void cU12UITest() {
		ViewInteraction appCompatEditText = onView(
			allOf(withId(R.id.usuario),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					0),
				isDisplayed()));
		appCompatEditText.perform(replaceText(USUARIO), closeSoftKeyboard());

		ViewInteraction appCompatEditText2 = onView(
			allOf(withId(R.id.usuario), withText(USUARIO),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					0),
				isDisplayed()));
		appCompatEditText2.perform(pressImeActionButton());

		ViewInteraction appCompatEditText3 = onView(
			allOf(withId(R.id.contraseña),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					1),
				isDisplayed()));
		appCompatEditText3.perform(replaceText(CONTRASEÑA), closeSoftKeyboard());

		ViewInteraction appCompatEditText4 = onView(
			allOf(withId(R.id.contraseña), withText(CONTRASEÑA),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					1),
				isDisplayed()));
		appCompatEditText4.perform(pressImeActionButton());

		ViewInteraction appCompatButton = onView(
			allOf(withId(R.id.login), withText(R.string.login_conectar),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					2),
				isDisplayed()));
		appCompatButton.perform(click());

		ViewInteraction tabView = onView(
			allOf(withContentDescription(R.string.pantalla_viajes),
				childAtPosition(
					childAtPosition(
						withId(R.id.contenedor_pestañas),
						0),
					1),
				isDisplayed()));
		tabView.perform(click());

		ViewInteraction recyclerView2 = onView(
			allOf(withId(R.id.rv_viajes),
				childAtPosition(
					withId(R.id.constraintLayout),
					1)));
		recyclerView2.perform(actionOnItemAtPosition(0, click()));

		ViewInteraction overflowMenuButton = onView(
			allOf(withContentDescription("Más opciones"),
				childAtPosition(
					childAtPosition(
						withId(R.id.toolbar),
						1),
					2),
				isDisplayed()));
		overflowMenuButton.perform(click());

		ViewInteraction appCompatTextView = onView(
			allOf(withId(R.id.title), withText(R.string.accion_borrar_viaje),
				childAtPosition(
					childAtPosition(
						withId(R.id.content),
						0),
					0),
				isDisplayed()));
		appCompatTextView.perform(click());

		ViewInteraction appCompatButton4 = onView(
			allOf(withId(android.R.id.button1), withText(R.string.viaje_btn_borrar_ok),
				childAtPosition(
					childAtPosition(
						withId(R.id.buttonPanel),
						0),
					3)));
		appCompatButton4.perform(scrollTo(), click());

		ViewInteraction tabView2 = onView(
			allOf(withContentDescription(R.string.pantalla_viajes),
				childAtPosition(
					childAtPosition(
						withId(R.id.contenedor_pestañas),
						0),
					1),
				isDisplayed()));
		tabView2.perform(click());

		ViewInteraction viewGroup = onView(
			allOf(withId(R.id.content_item_viaje),
				childAtPosition(withId(R.id.rv_viajes), 0),
				isDisplayed()));
		viewGroup.check(doesNotExist());
	}

	@After
	public void tearDown() throws Exception {
		ViajeDAO daoViaje = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoViaje();

		// Eliminar viaje de prueba por si no se hubiera completado el test
		daoViaje.borrar(ID_VIAJE_PRUEBA);
	}
}
