package es.unex.giiis.asee.transfugaz;


import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;

/**
 * Tests funcionales para el caso de uso CU07 - Listar usuarios del sistema
 * <p>
 * Comprueba que la aplicación muestre los 3 usuarios registrados por defecto: admin, tca y conductor
 */
@LargeTest
public class CU07UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule =
            new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";

    // Nombres de los usuarios por defecto
    private final int cuantosUsuariosPorDefecto = 3;
    private final String nombreAdmin = "admin";
    private final String nombreTCA = "tca";
    private final String nombreConductor = "conductor";

    @Test
    public void shouldShow3DefaultUsers() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.usuario), withText(usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.usuariosFragment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_inferior),
                                        0),
                                2),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        // ASEVERAR QUE HAY EXACTAMENTE 3 USUARIOS POR DEFECTO
        onView(withId(R.id.rv_usuarios)).check(matches(hasChildCount(cuantosUsuariosPorDefecto)));

        ViewInteraction textViewAdmin = onView(
                allOf(withId(R.id.nombreUsuario), withText(nombreAdmin),
                        withParent(withParent(withId(R.id.rv_usuarios))),
                        isDisplayed()));

        // ASEVERAR QUE EXISTE EL USUARIO ADMINISTRADOR POR DEFECTO "admin"
        textViewAdmin.check(matches(withText(nombreAdmin)));

        ViewInteraction textViewTCA = onView(
                allOf(withId(R.id.nombreUsuario), withText(nombreTCA),
                        withParent(withParent(withId(R.id.rv_usuarios))),
                        isDisplayed()));

        // ASEVERAR QUE EXISTE EL USUARIO TCA POR DEFECTO "tca"
        textViewTCA.check(matches(withText(nombreTCA)));


        ViewInteraction textViewConductor = onView(
                allOf(withId(R.id.nombreUsuario), withText(nombreConductor),
                        withParent(withParent(withId(R.id.rv_usuarios))),
                        isDisplayed()));

        // ASEVERAR QUE EXISTE EL USUARIO CONDUCTOR POR DEFECTO "conductor"
        textViewConductor.check(matches(withText(nombreConductor)));
    }

}
