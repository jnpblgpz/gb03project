package es.unex.giiis.asee.transfugaz;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;

import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.is;

@LargeTest
public class CU13UITest {

	@Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<>(MainActivity.class);

    private final String USUARIO = "admin";
    private final String CONTRASEÑA = "admin";
    private final String ID_VIAJE_PRUEBA = "TEST";

    @Before
    public void setUp() throws Exception {
        TransFugazDB roomDB = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext());
        ViajeDAO daoViaje = roomDB.getDaoViaje();
        Calendar fechaDescarga;

        // Viaje de prueba
        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1602579600);
        daoViaje.insertar(new Viaje(ID_VIAJE_PRUEBA, null, "origen", "destino", fechaDescarga,
            new ArrayList<String>(), 123, false));
    }

	@Test
	public void cU13UITest() {
        ViewInteraction appCompatEditText = onView(
            allOf(withId(R.id.usuario),
                childAtPosition(
                    allOf(withId(R.id.container),
                        childAtPosition(
                            withId(R.id.fragmentoPrincipal),
                            0)),
                    0),
                isDisplayed()));
        appCompatEditText.perform(replaceText(USUARIO), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
            allOf(withId(R.id.usuario), withText(USUARIO),
                childAtPosition(
                    allOf(withId(R.id.container),
                        childAtPosition(
                            withId(R.id.fragmentoPrincipal),
                            0)),
                    0),
                isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
            allOf(withId(R.id.contraseña),
                childAtPosition(
                    allOf(withId(R.id.container),
                        childAtPosition(
                            withId(R.id.fragmentoPrincipal),
                            0)),
                    1),
                isDisplayed()));
        appCompatEditText3.perform(replaceText(CONTRASEÑA), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
            allOf(withId(R.id.contraseña), withText(CONTRASEÑA),
                childAtPosition(
                    allOf(withId(R.id.container),
                        childAtPosition(
                            withId(R.id.fragmentoPrincipal),
                            0)),
                    1),
                isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
            allOf(withId(R.id.login), withText(R.string.login_conectar),
                childAtPosition(
                    allOf(withId(R.id.container),
                        childAtPosition(
                            withId(R.id.fragmentoPrincipal),
                            0)),
                    2),
                isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction tabView = onView(
            allOf(withContentDescription(R.string.pantalla_viajes),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.contenedor_pestañas),
                        0),
                    1),
                isDisplayed()));
        tabView.perform(click());

        ViewInteraction recyclerView2 = onView(
            allOf(withId(R.id.rv_viajes),
                childAtPosition(
                    withId(R.id.constraintLayout),
                    1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

		ViewInteraction actionMenuItemView = onView(
			allOf(withId(R.id.acc_crear_informe_finalizacion_viaje), withContentDescription(R.string.accion_informefinalizacion_viaje),
				childAtPosition(
					childAtPosition(
						withId(R.id.toolbar),
						1),
					0),
				isDisplayed()));
		actionMenuItemView.perform(click());

		ViewInteraction actionMenuItemView2 = onView(
			allOf(withId(R.id.acc_confirmar_informe_viaje), withContentDescription(R.string.accion_confirmar_edicion),
				childAtPosition(
					childAtPosition(
						withId(R.id.toolbar),
						1),
					0),
				isDisplayed()));
		actionMenuItemView2.perform(click());

		ViewInteraction textView = onView(
			allOf(withId(R.id.tv_viaje_detalle_estado),
				withParent(allOf(withId(R.id.contenido_detalle_viaje),
					withParent(withId(R.id.nestedScrollView))))
			));
		textView.check(matches(withText(R.string.viaje_estado_finalizado)));
	}

    @After
    public void tearDown() throws Exception {
        ViajeDAO daoViaje = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoViaje();

        // Eliminar viaje de prueba por si no se hubiera completado el test
        daoViaje.borrar(ID_VIAJE_PRUEBA);
    }
}
