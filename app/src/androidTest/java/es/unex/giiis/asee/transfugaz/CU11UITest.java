package es.unex.giiis.asee.transfugaz;


import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Calendar;

import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

/**
 * Pruebas funcionales para el CU11 - Editar viaje
 *
 * Comprueba que es posible editar los campos de un viaje precargado y que los cambios persistan
 * (omite la modificación de la fecha de descarga porque la aplicación utiliza widgets de terceros
 * que no interactúan con Espresso)
 */
@LargeTest
public class CU11UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule =
            new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";

    // Datos del viaje de prueba
    private final String idViaje = "SAUCAN13";
    private final String origen = "Sauce 20, 28942 Fuenlabrada, Madrid";
    private final String destino = "Can Fenosa 12, 08107 Barcelona";
    private final String mercancia1 = "Gigabyte B450M DS3H";
    private final String mercancia2 = "Zotac GAMING GeForce RTX2060 6GB GDDR6";
    private final float distancia = 506.685f;

    // Nuevos datos con los que editar el viaje de prueba
    private final String origenPrueba = "OrigenPrueba";
    private final String destinoPrueba = "DestinoPrueba";
    private final String mercanciaPrueba = "ItemPrueba";
    private final String distanciaPrueba = "1";

    @Before
    public void setUp() throws Exception {
        Calendar fechaDescarga;
        TransFugazDB roomDB = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext());
        ViajeDAO daoViaje = roomDB.getDaoViaje();
        UsuarioDAO daoUsuario = roomDB.getDaoUsuario();

        Long idTca = daoUsuario.getPorNombreNLD("tca").getId();

        // Viaje de prueba

        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1602579600);
        // Lo conduce el TCA
        Viaje viajePrueba = new Viaje(idViaje, idTca, origen,
                destino, fechaDescarga,
                Arrays.asList(mercancia1, mercancia2),
                distancia, false);

        // Insertar el viaje de prueba

        daoViaje.insertar(viajePrueba);
    }

    @Test
    public void shouldEditPreloadedJourney() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.usuario), withText(usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction tabView = onView(allOf(
                childAtPosition(
                        childAtPosition(
                                withId(R.id.contenedor_pestañas),
                                0),
                        1),
                isDisplayed()));
        tabView.perform(click());

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.rv_viajes),
                        childAtPosition(
                                withId(R.id.constraintLayout),
                                1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.acc_editar_viaje),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                1),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction appCompatSpinner = onView(
                allOf(withId(R.id.spinner_viaje_conductor),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                9),
                        isDisplayed()));
        appCompatSpinner.perform(click());

        DataInteraction appCompatCheckedTextView = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(0);
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.et_viaje_origen),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                10),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText(origenPrueba));

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.et_viaje_origen),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                10),
                        isDisplayed()));
        appCompatEditText6.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.et_viaje_origen),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                10),
                        isDisplayed()));
        appCompatEditText7.perform(pressImeActionButton());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.et_viaje_destino),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText9.perform(replaceText(destinoPrueba), closeSoftKeyboard());

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.et_viaje_destino),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText11.perform(pressImeActionButton());

        onView(withId(android.R.id.content)).perform(ViewActions.swipeUp());

        ViewInteraction appCompatEditText12 = onView(withId(R.id.et_viaje_agragarMercancia));
        appCompatEditText12.perform(replaceText(mercanciaPrueba), closeSoftKeyboard());

        ViewInteraction appCompatButton8 = onView(withId(R.id.btn_viaje_agregarMercancia));
        appCompatButton8.perform(click());

        onView(withId(android.R.id.content)).perform(ViewActions.swipeUp());

        ViewInteraction appCompatEditText13 = onView(
                allOf(withId(R.id.et_viaje_distancia),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText13.perform(replaceText(distanciaPrueba));

        ViewInteraction appCompatEditText14 = onView(
                allOf(withId(R.id.et_viaje_distancia),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText14.perform(closeSoftKeyboard());

        ViewInteraction appCompatCheckBox = onView(
                allOf(withId(R.id.ckbx_viaje_finalizado), isDisplayed()));
        appCompatCheckBox.perform(click());

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.acc_confirmar_edicion_viaje),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView2.perform(click());

        // ASEVERAR QUE EL CAMPO "ORIGEN" TENGA EL VALOR EDITADO
        ViewInteraction textView = onView(
                allOf(withId(R.id.tv_viaje_detalle_origen),
                        withParent(allOf(withId(R.id.contenido_detalle_viaje),
                                withParent(withId(R.id.nestedScrollView)))),
                        isDisplayed()));
        textView.check(matches(withText(origenPrueba)));

        // ASEVERAR QUE EL CAMPO "DESTINO" TENGA EL VALOR EDITADO
        ViewInteraction textView2 = onView(
                allOf(withId(R.id.tv_viaje_detalle_destino),
                        withParent(allOf(withId(R.id.contenido_detalle_viaje),
                                withParent(withId(R.id.nestedScrollView)))),
                        isDisplayed()));
        textView2.check(matches(withText(destinoPrueba)));

        // ASEVERAR QUE EL CAMPO "DISTANCIA" TENGA EL VALOR EDITADO
        ViewInteraction textView3 = onView(
                allOf(withId(R.id.tv_viaje_detalle_distancia),
                        withParent(allOf(withId(R.id.contenido_detalle_viaje),
                                withParent(withId(R.id.nestedScrollView)))),
                        isDisplayed()));
        textView3.check(matches(withText(distanciaPrueba + ".0")));

        // ASEVERAR QUE EL NUEVO CONDUCTOR ASIGNADO SEA "CONDUCTOR"
        ViewInteraction textView4 = onView(
                allOf(withId(R.id.tv_viaje_detalle_conductor),
                        withParent(allOf(withId(R.id.contenido_detalle_viaje),
                                withParent(withId(R.id.nestedScrollView)))),
                        isDisplayed()));
        textView4.check(matches(withText("conductor")));

        onView(withId(android.R.id.content)).perform(ViewActions.swipeUp());

        // ASEVERAR QUE LA PARTIDA DE MERCANCÍA CONTENGA 3 ÍTEMS, LOS DOS ORIGINALES Y EL DE PRUEBA
        onView(withId(R.id.rv_viaje_detalle_mercancia)).check(matches(hasChildCount(3)));

        // ASEVERAR QUE SIGA EXISTIENDO EL PRIMER ÍTEM DE LA MERCANCIA
        ViewInteraction textView5 = onView(
                allOf(withId(R.id.tv_viaje_mercancia_item), withText(mercancia1),
                        withParent(withParent(withId(R.id.rv_viaje_detalle_mercancia))),
                        isDisplayed()));
        textView5.check(matches(isDisplayed()));

        // ASEVERAR QUE SIGA EXISTIENDO EL SEGUNDO ÍTEM DE LA MERCANCIA
        ViewInteraction textView55 = onView(
                allOf(withId(R.id.tv_viaje_mercancia_item), withText(mercancia2),
                        withParent(withParent(withId(R.id.rv_viaje_detalle_mercancia))),
                        isDisplayed()));
        textView5.check(matches(isDisplayed()));

        // ASEVERAR QUE EXISTA UN NUEVO ITEM EN LA MERCANCÍA CON EL NOMBRE DEL ITEM DE PRUEBA
        ViewInteraction textView6 = onView(
                allOf(withId(R.id.tv_viaje_mercancia_item), withText(mercanciaPrueba),
                        withParent(withParent(withId(R.id.rv_viaje_detalle_mercancia))),
                        isDisplayed()));
        textView6.check(matches(isDisplayed()));

        // ASEVERAR QUE EL NUEVO ESTADO DEL VIAJE ES "FINALIZADO"
        ViewInteraction textView7 = onView(
                allOf(withId(R.id.tv_viaje_detalle_estado),
                        withParent(allOf(withId(R.id.contenido_detalle_viaje),
                                withParent(withId(R.id.nestedScrollView)))),
                        isDisplayed()));
        textView7.check(matches(withText("FINALIZADO")));
    }

    @After
    public void tearDown() throws Exception {
        ViajeDAO daoViaje =
                TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoViaje();

        // Eliminar el viaje de prueba
        daoViaje.borrar(idViaje);
    }
}
