package es.unex.giiis.asee.transfugaz.roomdb;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.testUtils.LiveDataTestUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ViajeDAOTest {

    private TransFugazDB db;
    private ViajeDAO daoViaje;
    private UsuarioDAO daoUsuario;
    private ArrayList<Viaje> viajesPrueba;

    // Identificadores de los viajes de prueba
    private final String idViaje1 = "SAUCAN13";
    private final String idViaje2 = "ANARUA11";
    // Identificadores de los conductores
    private final long idConductor1 = 1L;
    private final long idConductor2 = 2L;
    // Viajes de prueba
    Viaje viaje1;
    Viaje viaje2;

    @Rule
    public InstantTaskExecutorRule executorRule = new InstantTaskExecutorRule();

    @Before
    public void crearDB() {
        Context contexto = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(contexto, TransFugazDB.class).allowMainThreadQueries().build();
        daoViaje = db.getDaoViaje();
        daoUsuario = db.getDaoUsuario();

        Calendar fechaDescarga;

        viajesPrueba = new ArrayList<>();
        List<String> mercanciaPrueba = Arrays.asList("m1", "m2", "m3");

        // Conductores de prueba

        daoUsuario.insertar(new Usuario(1L, "conductor1", "1234", Rol.CONDUCTOR));
        daoUsuario.insertar(new Usuario(2L, "conductor2", "1234", Rol.CONDUCTOR));
        daoUsuario.insertar(new Usuario(3L, "conductor3", "1234", Rol.CONDUCTOR));

        // Viajes de prueba

        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1602579600);
        // Asignado al conductor 1
        viaje1 = new Viaje(idViaje1, idConductor1, "Sauce 20, 28942 Fuenlabrada, Madrid",
                "Can Fenosa 12, 08107 Barcelona", fechaDescarga,
                Arrays.asList("Gigabyte B450M DS3H", "Zotac GAMING GeForce RTX2060 6GB GDDR6"),
                506.685f, false);

        fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1605078000);
        // Asociado al conductor 2
        viaje2 = new Viaje(idViaje2, idConductor2, "Ana Mariscal, 10004 Cáceres",
                "Rua Zapatería, 10600 Plasencia, Cáceres", fechaDescarga,
                Arrays.asList("Kingston A400 SSD 240GB", "MSI B450 Gaming Plus MAX"),
                67.93f, false);

        viajesPrueba.add(viaje1);
        viajesPrueba.add(viaje2);

        // Insertar los viajes de prueba en la BD de Room en memoria

        daoViaje.insertar(viaje1);
        daoViaje.insertar(viaje2);
    }

    @Test
    public void getTodosTest() throws InterruptedException {
        assertEquals(viajesPrueba, LiveDataTestUtil.getValue(daoViaje.getTodos()));
    }

    @Test
    public void getPorConductorTest() throws InterruptedException {
        assertEquals(Arrays.asList(viaje1), LiveDataTestUtil.getValue(daoViaje.getPorConductor(idConductor1)));
        assertEquals(Arrays.asList(viaje2), LiveDataTestUtil.getValue(daoViaje.getPorConductor(idConductor2)));
    }

    @Test
    public void getPorIdTest() throws InterruptedException {
        assertEquals(viajesPrueba.get(0), LiveDataTestUtil.getValue(daoViaje.getPorId(idViaje1)));
        assertEquals(viajesPrueba.get(1), LiveDataTestUtil.getValue(daoViaje.getPorId(idViaje2)));
    }

    @Test
    public void insertarTest() throws InterruptedException {
        Calendar fechaDescarga = Calendar.getInstance();
        fechaDescarga.setTimeInMillis(1602579600);
        String id = "333";
        Viaje viajePrueba = new Viaje(id, 3L, "Sauce 20, 28942 Fuenlabrada, Madrid",
                "Can Fenosa 12, 08107 Barcelona", fechaDescarga,
                Arrays.asList("Gigabyte B450M DS3H", "Zotac GAMING GeForce RTX2060 6GB GDDR6"),
                506.685f, false);

        daoViaje.insertar(viajePrueba);

        assertEquals(viajePrueba, LiveDataTestUtil.getValue(daoViaje.getPorId(id)));
    }

    @Test
    public void actualizarTest() throws InterruptedException {
        viaje1.setFinalizado(true);

        daoViaje.actualizar(viaje1);

        assertTrue(LiveDataTestUtil.getValue(daoViaje.getPorId(idViaje1)).isFinalizado());
    }

    @Test
    public void borrarTest() throws InterruptedException {
        daoViaje.borrar(idViaje1);
        daoViaje.borrar(idViaje2);

        assertEquals(0, LiveDataTestUtil.getValue(daoViaje.getTodos()).size());
    }

    @After
    public void cerrarDB() {
        db.close();
    }
}