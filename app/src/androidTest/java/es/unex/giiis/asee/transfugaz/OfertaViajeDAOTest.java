package es.unex.giiis.asee.transfugaz;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.testUtils.LiveDataTestUtil;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class OfertaViajeDAOTest {
	private TransFugazDB db;
	private OfertaViajeAgregadoDAO dao;
	private ArrayList<OfertaViaje> ofertasDePrueba;

	@Rule
	public InstantTaskExecutorRule executorRule = new InstantTaskExecutorRule();

	@Before
	public void crearDB() {
		Context contexto = InstrumentationRegistry.getInstrumentation().getTargetContext();
		db = Room.inMemoryDatabaseBuilder(contexto, TransFugazDB.class).allowMainThreadQueries().build();
		dao = db.getDaoOfertaViajeAgregado();

		ofertasDePrueba = new ArrayList<>();
		ArrayList<String> mercancíaDePrueba = new ArrayList<>();
		mercancíaDePrueba.add("m1");
		mercancíaDePrueba.add("m2");
		mercancíaDePrueba.add("m3");

		ofertasDePrueba.add(new OfertaViaje(123, "cliente", 1234, "origen",
			4321, "destino", mercancíaDePrueba, 100, 500));
		ofertasDePrueba.add(new OfertaViaje(222, "cliente2", 1224, "origen2",
			4221, "destino2", mercancíaDePrueba, 101, 510));
		ofertasDePrueba.add(new OfertaViaje(333, "cliente3", 1334, "origen3",
			4331, "destino3", mercancíaDePrueba, 102, 520));
		dao.insertarOfertasPorLotes(ofertasDePrueba);
	}

	@After
	public void cerrarDB() {
		db.close();
	}

	@Test
	public void getTodasTest() throws InterruptedException {
		assertEquals(ofertasDePrueba, LiveDataTestUtil.getValue(dao.getTodasOfertas()));
	}

	@Test
	public void getPorIdTest() throws InterruptedException {
		assertEquals(ofertasDePrueba.get(0), LiveDataTestUtil.getValue(dao.getOfertaPorId(123)));
	}

	@Test
	public void cuantasOfertasTest() {
		assertEquals(3, dao.cuantasOfertas());
	}

	@Test
	public void borrarOfertasTest() throws InterruptedException {
		dao.borrarTodasOfertas();
		assertEquals(0, dao.cuantasOfertas());
		assertEquals(new ArrayList<OfertaViaje>(), LiveDataTestUtil.getValue(dao.getTodasOfertas()));
	}
}