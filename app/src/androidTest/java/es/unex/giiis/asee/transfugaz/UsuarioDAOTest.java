package es.unex.giiis.asee.transfugaz;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.testUtils.LiveDataTestUtil;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class UsuarioDAOTest {
	private TransFugazDB db;
	private UsuarioDAO dao;
	private ArrayList<Usuario> usuariosDePrueba;

	@Rule
	public InstantTaskExecutorRule executorRule = new InstantTaskExecutorRule();

	@Before
	public void crearDB() {
		Context contexto = InstrumentationRegistry.getInstrumentation().getTargetContext();
		db = Room.inMemoryDatabaseBuilder(contexto, TransFugazDB.class).allowMainThreadQueries().build();
		dao = db.getDaoUsuario();

		usuariosDePrueba = new ArrayList<>();

		usuariosDePrueba.add(new Usuario(111L, "usuario1", "pass1", Rol.CONDUCTOR));
		usuariosDePrueba.add(new Usuario(222L, "usuario2", "pass2", Rol.CONDUCTOR));
		usuariosDePrueba.add(new Usuario(333L, "usuario3", "pass3", Rol.TCA));

		dao.insertar(usuariosDePrueba.get(0));
		dao.insertar(usuariosDePrueba.get(1));
		dao.insertar(usuariosDePrueba.get(2));
	}

	@After
	public void cerrarDB() {
		db.close();
	}

	@Test
	public void getTodosTest() throws InterruptedException {
		assertEquals(usuariosDePrueba, LiveDataTestUtil.getValue(dao.getTodos()));
	}

	@Test
	public void getPorIdTest() throws InterruptedException {
		assertEquals(usuariosDePrueba.get(0), LiveDataTestUtil.getValue(dao.getPorId(111L)));
	}

	@Test
	public void getPorNombreTest() throws InterruptedException {
		assertEquals(usuariosDePrueba.get(0), LiveDataTestUtil.getValue(dao.getPorNombre("usuario1")));
	}

	@Test
	public void getPorRolTest() throws InterruptedException {
		// El último usuario es TCA así que no debería incluirse
		usuariosDePrueba.remove(2);
		assertEquals(usuariosDePrueba, LiveDataTestUtil.getValue(dao.getPorRol(Rol.CONDUCTOR.ordinal())));
	}

	@Test
	public void getPorRolNLDTest() throws InterruptedException {
		// El último usuario es TCA así que no debería incluirse
		usuariosDePrueba.remove(2);
		assertEquals(usuariosDePrueba, dao.getPorRolNLD(Rol.CONDUCTOR.ordinal()));
	}

	@Test
	public void getPorIdNLDTest() throws InterruptedException {
		assertEquals(usuariosDePrueba.get(0), dao.getPorIdNLD(111L));
	}

	@Test
	public void getPorNombreNLDTest() throws InterruptedException {
		assertEquals(usuariosDePrueba.get(0), dao.getPorNombreNLD("usuario1"));
	}

	@Test
	public void getNombresPorRolTest() throws InterruptedException {
		List<String> resultadoEsperado = new ArrayList<String>();
		resultadoEsperado.add("usuario1");
		resultadoEsperado.add("usuario2");
		assertEquals(resultadoEsperado, dao.getNombresPorRol(Rol.CONDUCTOR.ordinal()));
	}

	@Test
	public void actualizarUsuarioTest() {
		Usuario usuario = usuariosDePrueba.get(0);
		usuario.setRol(Rol.ADMIN);
		dao.actualizar(usuario);

		assertEquals(usuario, dao.getPorIdNLD(111L));
	}

	@Test
	public void borrarUsuarioPorIdTest() throws InterruptedException {
		dao.borrar(111L);
		usuariosDePrueba.remove(0);

		assertEquals(usuariosDePrueba, LiveDataTestUtil.getValue(dao.getTodos()));
	}

	@Test
	public void borrarUsuarioPorNombreTest() throws InterruptedException {
		dao.borrar("usuario2");
		usuariosDePrueba.remove(1);

		assertEquals(usuariosDePrueba, LiveDataTestUtil.getValue(dao.getTodos()));
	}
}