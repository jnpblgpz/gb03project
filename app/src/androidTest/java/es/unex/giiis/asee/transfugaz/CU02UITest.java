package es.unex.giiis.asee.transfugaz;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;

@LargeTest
public class CU02UITest {

	@Rule
	public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    private final String USUARIO = "admin";
    private final String CONTRASEÑA = "admin";
    private final String CLIENTE_1 = "Logística de Extremadura SL";
	private final String CLIENTE_2 = "Fedex";

	@Before
	public void setUp() throws Exception {
		TransFugazDB roomDB = TransFugazDB.getInstance(ApplicationProvider.getApplicationContext());
		OfertaViajeAgregadoDAO daoOferta = roomDB.getDaoOfertaViajeAgregado();

		// Ofertas de prueba
		List<OfertaViaje> ofertas = new ArrayList<>();

		ofertas.add(new OfertaViaje(123, CLIENTE_1, 1602579600, "origen",
			1602582600, "destino", null, 123, 431));
		ofertas.add(new OfertaViaje(311, CLIENTE_1, 1602579600, "origen",
			1602582600, "destino", null, 123, 431));
		ofertas.add(new OfertaViaje(333, CLIENTE_2, 1602579600, "origen",
			1602582600, "destino", null, 123, 431));

		// Insertar ofertas de prueba en la BD
		daoOferta.insertarOfertasPorLotes(ofertas);
		daoOferta.insertarOfertaBD(new OfertaViaje.OfertaViajeDB(123, EstadoOferta.PENDIENTE));
		daoOferta.insertarOfertaBD(new OfertaViaje.OfertaViajeDB(311, EstadoOferta.PENDIENTE));
		daoOferta.insertarOfertaBD(new OfertaViaje.OfertaViajeDB(333, EstadoOferta.PENDIENTE));
	}

	@Test
	public void cU02UITest() {
		ViewInteraction appCompatEditText = onView(
			allOf(withId(R.id.usuario),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					0),
				isDisplayed()));
		appCompatEditText.perform(replaceText(USUARIO), closeSoftKeyboard());

		ViewInteraction appCompatEditText2 = onView(
			allOf(withId(R.id.usuario), withText(USUARIO),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					0),
				isDisplayed()));
		appCompatEditText2.perform(pressImeActionButton());

		ViewInteraction appCompatEditText3 = onView(
			allOf(withId(R.id.contraseña),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					1),
				isDisplayed()));
		appCompatEditText3.perform(replaceText(CONTRASEÑA), closeSoftKeyboard());

		ViewInteraction appCompatEditText4 = onView(
			allOf(withId(R.id.contraseña), withText(CONTRASEÑA),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					1),
				isDisplayed()));
		appCompatEditText4.perform(pressImeActionButton());

		ViewInteraction appCompatButton = onView(
			allOf(withId(R.id.login), withText(R.string.login_conectar),
				childAtPosition(
					allOf(withId(R.id.container),
						childAtPosition(
							withId(R.id.fragmentoPrincipal),
							0)),
					2),
				isDisplayed()));
		appCompatButton.perform(click());

		ViewInteraction viewGroup = onView(
			allOf(withId(R.id.content_item_oferta),
				childAtPosition(withId(R.id.rv_ofertasviaje), 0),
				isDisplayed()));
		viewGroup.check(matches(isDisplayed()));

		ViewInteraction viewGroup2 = onView(
			allOf(withId(R.id.content_item_oferta),
				childAtPosition(withId(R.id.rv_ofertasviaje), 1),
				isDisplayed()));
		viewGroup2.check(matches(isDisplayed()));

		ViewInteraction viewGroup3 = onView(
			allOf(withId(R.id.content_item_oferta),
				childAtPosition(withId(R.id.rv_ofertasviaje), 2),
				isDisplayed()));
		viewGroup3.check(matches(isDisplayed()));

		ViewInteraction textView = onView(
			allOf(withId(R.id.tv_oferta_cliente),
				withParent(allOf(withId(R.id.content_item_oferta),
					childAtPosition(withId(R.id.rv_ofertasviaje), 0),
					isDisplayed()))));
		textView.check(matches(withText(CLIENTE_1)));

		ViewInteraction textView2 = onView(
			allOf(withId(R.id.tv_oferta_cliente),
				withParent(allOf(withId(R.id.content_item_oferta),
					childAtPosition(withId(R.id.rv_ofertasviaje), 1),
					isDisplayed()))));
		textView2.check(matches(withText(CLIENTE_1)));

		ViewInteraction textView3 = onView(
			allOf(withId(R.id.tv_oferta_cliente),
				withParent(allOf(withId(R.id.content_item_oferta),
					childAtPosition(withId(R.id.rv_ofertasviaje), 2),
					isDisplayed()))));
		textView3.check(matches(withText(CLIENTE_2)));
	}

	@After
	public void tearDown() throws Exception {
		OfertaViajeAgregadoDAO daoOferta =
			TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoOfertaViajeAgregado();

		// Eliminar las ofertas de prueba
		daoOferta.borrarTodasOfertas();
		daoOferta.borrarOfertaBD(123);
		daoOferta.borrarOfertaBD(311);
		daoOferta.borrarOfertaBD(333);
	}
}
