package es.unex.giiis.asee.transfugaz;


import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.hasSibling;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

/**
 * Pruebas funcionales para el CU08 - Editar usuario
 *
 * Comprueba que es posible editar el nombre de un usuario y cambiar su rol
 */
@LargeTest
public class CU08UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule =
            new ActivityScenarioRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";
    // Nombre del usuario que se va a editar
    private final String nombreAntiguoUsuarioEditado = "tca";
    private final String claveAntiguaUsuarioEditado = "tca";
    // Nuevos datos del usuario editado
    private final String nuevoNombreUsuarioEditado = "NuevoAdmin";
    private final String nuevaClaveUsuarioEditado = "1234";

    @Test
    public void shouldEditDefaultTcaUser() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.usuario), withText(usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.usuariosFragment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_inferior),
                                        0),
                                2),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.editarUsuario),
                        hasSibling(withText(nombreAntiguoUsuarioEditado)),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.et_nombreUsuario), withText(nombreAntiguoUsuarioEditado),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText(nuevoNombreUsuarioEditado));

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.et_nombreUsuario), withText(nuevoNombreUsuarioEditado),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText6.perform(closeSoftKeyboard());

        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.tiet_pwd),
                        isDisplayed()));
        textInputEditText.perform(replaceText(nuevaClaveUsuarioEditado), closeSoftKeyboard());

        ViewInteraction textInputEditText2 = onView(
                allOf(withId(R.id.tiet_pwd), withText(nuevaClaveUsuarioEditado),
                        isDisplayed()));
        textInputEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatSpinner = onView(
                allOf(withId(R.id.spinner_rol),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.core.widget.NestedScrollView")),
                                        0),
                                3),
                        isDisplayed()));
        appCompatSpinner.perform(click());

        DataInteraction appCompatCheckedTextView = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(0);
        appCompatCheckedTextView.perform(click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.acc_confirmar_edicion_usuario),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        // ASERVERAR QUE EL USUARIO EDITADO EXISTE Y EL NOMBRE ES EL EDITADO
        onView(withText(nuevoNombreUsuarioEditado)).check(matches(isCompletelyDisplayed()));

        // ASEVERAR QUE EL USUARIO EDITADO TIENE AHORA EL ROL ADMIN
        onView(allOf(withId(R.id.rolUsuario),
                withParent(withParent(withId(R.id.rv_usuarios))),
                withParent(hasDescendant(withText(nuevoNombreUsuarioEditado))),
                isDisplayed()))
                .check(matches(withText(Rol.ADMIN.toString())));

        // ASEVERAR QUE YA NO SE MUESTRA EL NOMBRE ANTIGUO
        onView(withText(nombreAntiguoUsuarioEditado)).check(doesNotExist());
    }

    @After
    public void tearDown() {
        UsuarioDAO daoUsuario =
                TransFugazDB.getInstance(ApplicationProvider.getApplicationContext()).getDaoUsuario();

        // Recuperar al usuario editado por su nuevo nombre
        Usuario usuarioRestaurado = daoUsuario.getPorNombreNLD(nuevoNombreUsuarioEditado);

        // Restaurar el usuario editado a su estado anterior
        usuarioRestaurado.setNombre(nombreAntiguoUsuarioEditado);
        usuarioRestaurado.setContraseña(claveAntiguaUsuarioEditado);
        daoUsuario.actualizar(usuarioRestaurado);
    }
}
