package es.unex.giiis.asee.transfugaz;


import android.content.pm.ActivityInfo;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.hasSibling;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static es.unex.giiis.asee.transfugaz.testUtils.TestUtils.childAtPosition;
import static org.hamcrest.Matchers.allOf;

/**
 * Test funcional para probar el CU15 - Borrar a un usuario del sistema
 *
 * Comprueba que es posible borrar a un usuario utilizando un usuario ya registrado por defecto
 * en el sistema
 */
@LargeTest
public class CU15UITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    // Parámetros del test

    // Credenciales
    private final String usuario = "admin";
    private final String clave = "admin";

    // A qué usuario hay que eliminar
    private final String usuarioEliminar = "conductor";
    // Cuántos usuarios hay por defecto
    private final int cuantosUsuariosPorDefecto = 3;

    @Test
    public void shouldDeleteUserConductor() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(usuario), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.usuario), withText(usuario),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.contraseña),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText(clave), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.contraseña), withText(clave),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login),
                        childAtPosition(
                                allOf(withId(R.id.container),
                                        childAtPosition(
                                                withId(R.id.fragmentoPrincipal),
                                                0)),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.usuariosFragment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_inferior),
                                        0),
                                2),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        mActivityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.borrarUsuario), hasSibling(withText(usuarioEliminar)),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(android.R.id.button1),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        appCompatButton2.perform(scrollTo(), click());

        mActivityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // ASEVERAR QUE LA LISTA DE USUARIOS AHORA CONTIENE EL NÚMERO DE USUARIOS POR DEFECTO MENOS 1
        onView(withId(R.id.rv_usuarios)).check(matches(hasChildCount(cuantosUsuariosPorDefecto - 1)));

        // ASEVERAR QUE YA NO SE PUEDE ENCONTRAR A UN CONDUCTOR POR EL NOMBRE DEL CONDUCTOR ELIMINADO
        onView(withText(usuarioEliminar)).check(doesNotExist());
    }
}
