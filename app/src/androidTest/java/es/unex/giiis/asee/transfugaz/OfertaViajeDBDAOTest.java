package es.unex.giiis.asee.transfugaz;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.testUtils.LiveDataTestUtil;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class OfertaViajeDBDAOTest {
	private TransFugazDB db;
	private OfertaViajeAgregadoDAO dao;
	private ArrayList<OfertaViaje.OfertaViajeDB> ofertasDePrueba;

	@Rule
	public InstantTaskExecutorRule executorRule = new InstantTaskExecutorRule();

	@Before
	public void crearDB() {
		Context contexto = InstrumentationRegistry.getInstrumentation().getTargetContext();
		db = Room.inMemoryDatabaseBuilder(contexto, TransFugazDB.class).allowMainThreadQueries().build();
		dao = db.getDaoOfertaViajeAgregado();

		ofertasDePrueba = new ArrayList<>();
		ofertasDePrueba.add(new OfertaViaje.OfertaViajeDB(123, EstadoOferta.ACEPTADA));
		ofertasDePrueba.add(new OfertaViaje.OfertaViajeDB(222, EstadoOferta.RECHAZADA));
		ofertasDePrueba.add(new OfertaViaje.OfertaViajeDB(333, EstadoOferta.PENDIENTE));

		dao.insertarOfertaBD(ofertasDePrueba.get(0));
		dao.insertarOfertaBD(ofertasDePrueba.get(1));
		dao.insertarOfertaBD(ofertasDePrueba.get(2));
	}

	@After
	public void cerrarDB() {
		db.close();
	}

	@Test
	public void getTodasTest() throws InterruptedException {
		assertEquals(ofertasDePrueba, LiveDataTestUtil.getValue(dao.getTodasOfertasBD()));
	}

	@Test
	public void getTodasNLDTest() {
		assertEquals(ofertasDePrueba, dao.getTodasOfertasBD_NLD());
	}

	@Test
	public void getPorIdTest() throws InterruptedException {
		assertEquals(ofertasDePrueba.get(0), LiveDataTestUtil.getValue(dao.getOfertaBDPorId(123)));
	}

	@Test
	public void getPorIdNLDTest() {
		assertEquals(ofertasDePrueba.get(0), dao.getOfertaBDPorId_NLD(123));
	}

	@Test
	public void actualizarOfertaTest() {
		OfertaViaje.OfertaViajeDB oferta = ofertasDePrueba.get(0);
		oferta.setEstado(EstadoOferta.RECHAZADA);
		dao.actualizarOfertaBD(oferta);

		assertEquals(oferta, dao.getOfertaBDPorId_NLD(123));
	}

	@Test
	public void borrarOfertaTest() {
		dao.borrarOfertaBD(123);
		ofertasDePrueba.remove(0);

		assertEquals(2, dao.getTodasOfertasBD_NLD().size());
		assertEquals(ofertasDePrueba, dao.getTodasOfertasBD_NLD());
	}
}