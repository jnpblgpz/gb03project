package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.room.TypeConverter;

import java.util.Calendar;

/**
 * Conversor de tipo Room para traducir un sello de tiempo en un objeto java.util.Calendar y viceversa
 */
public class CalendarConverter {

    @TypeConverter
    public static Long toTimestamp(Calendar calendar) {
        return (calendar == null) ? null : calendar.getTimeInMillis() / 1000; // Conversión a segundos
    }

    @TypeConverter
    public static Calendar toCalendar(Long timestamp) {

        if (timestamp == null) return null;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp * 1000); // Conversión a milisegundos

        return calendar;
    }

}
