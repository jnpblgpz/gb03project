package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.RolConverter;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;

public class LoginFragment extends Fragment {
	private final String NOMBRE_ADMIN_DEFAULT = "admin";
	private final String PASS_ADMIN_DEFAULT = "admin";
	private final String NOMBRE_TCA_DEFAULT = "tca";
	private final String PASS_TCA_DEFAULT = "tca";
	private final String NOMBRE_CONDUCTOR_DEFAULT = "conductor";
	private final String PASS_CONDUCTOR_DEFAULT = "test";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AppExecutors.getInstance().diskIO().execute(() -> {
			// Si no hay ningún usuario registrado con cada uno de los 3 roles, crear unos por defecto
			registrarSiNoExiste(Rol.ADMIN, NOMBRE_ADMIN_DEFAULT, PASS_ADMIN_DEFAULT);
			registrarSiNoExiste(Rol.TCA, NOMBRE_TCA_DEFAULT, PASS_TCA_DEFAULT);
			registrarSiNoExiste(Rol.CONDUCTOR, NOMBRE_CONDUCTOR_DEFAULT, PASS_CONDUCTOR_DEFAULT);
		});
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
							 @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_login, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final EditText usernameEditText = view.findViewById(R.id.usuario);
		final EditText passwordEditText = view.findViewById(R.id.contraseña);
		final Button loginButton = view.findViewById(R.id.login);
		final TextView textoError = view.findViewById(R.id.msgErrorLogin);
		textoError.setVisibility(View.INVISIBLE);

		loginButton.setOnClickListener(v -> AppExecutors.getInstance().diskIO().execute(() -> {
			String usuario = usernameEditText.getText().toString();
			String contraseña = passwordEditText.getText().toString();
			TransFugazDB bd = TransFugazDB.getInstance(getContext());
			UsuarioDAO dao = bd.getDaoUsuario();
			Usuario usuarioRecuperado = dao.getPorNombreNLD(usuario);

			if (usuarioRecuperado != null) {
				if (usuarioRecuperado.getContraseña().equals(contraseña)) {
					// Almacenar datos del usuario conectado
					((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado = usuarioRecuperado;

					getActivity().runOnUiThread(() -> {
						textoError.setVisibility(View.INVISIBLE);
						NavController navController = Navigation.findNavController(view);
						navController.navigate(R.id.navegacion_login_a_transporte);
					});

				} else {
					// Contraseña incorrecta
					getActivity().runOnUiThread(() -> {
						textoError.setText(R.string.contraseña_incorrecta);
						textoError.setVisibility(View.VISIBLE);
					});
				}
			} else {
				// El usuario no existe
				getActivity().runOnUiThread(() -> {
					textoError.setText(R.string.usuario_no_existe);
					textoError.setVisibility(View.VISIBLE);
				});
			}
		}));
	}

	/**
	 * Comprueba si existe al menos un usuario con el rol indicado y si no es así, crea uno con el usuario y la
	 * contraseña especificados (Si ya existe uno con ese nombre de usuario, se sobrescribe). El nuevo usuario tendrá el mismo
	 * rol que el especificado.
	 * Debe ser llamado dentro de un Runnable de DiskIO.
	 * @param rol Rol a comprobar
	 * @param nombreUsuario Nombre del usuario a crear si no existe ninguno con ese rol
	 * @param contraseña Contraseña del usuario a crear si no existe ninguno con ese rol
	 */
	private void registrarSiNoExiste(Rol rol, String nombreUsuario, String contraseña) {
		TransFugazDB bd = TransFugazDB.getInstance(getContext());
		UsuarioDAO dao = bd.getDaoUsuario();
		List<Usuario> usuariosConRol = dao.getPorRolNLD(RolConverter.toInteger(rol));
		if (usuariosConRol.size() == 0) {
			Usuario nuevo = new Usuario(null, nombreUsuario, contraseña, rol);
			// Borrar posibles usuarios que ya existan con este nombre y luego insertar
			dao.borrar(nombreUsuario);
			dao.insertar(nuevo);
		}
	}
}