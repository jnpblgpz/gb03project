package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.vm.ListaUsuariosVM;

public class UsuariosFragment extends Fragment {
	private RecyclerView recyclerView;
	private RecyclerView.LayoutManager layoutManager;
	private AdaptadorUsuario adaptadorUsuario;

	private ListaUsuariosVM listaUsuariosVM;

	public UsuariosFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		// Obtener el VM correspondiente a este fragmento
		AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
		listaUsuariosVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(ListaUsuariosVM.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_usuarios, container, false);
	}

	public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Preparar la RecyclerView y el adaptador
		recyclerView = view.findViewById(R.id.rv_usuarios);
		recyclerView.setHasFixedSize(true);
		layoutManager = new LinearLayoutManager(getActivity());
		recyclerView.setLayoutManager(layoutManager);
		adaptadorUsuario = new AdaptadorUsuario((MainActivity) getActivity(), this);
		recyclerView.setAdapter(adaptadorUsuario);

		// Observar la lista de usuarios
		listaUsuariosVM.getUsuarios().observe(getViewLifecycleOwner(), this::alCargarUsuarios);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_toolbar_listausuarios, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == R.id.acc_añadir_usuario) {
			añadirUsuario();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	/**
	 * Llamado por el LiveData de usuarios cuando se actualizan los usuarios de la base de datos.
	 * Encargado de pasar la nueva lista de usuarios al adaptador
	 * @param usuarios Nueva lista de usuarios
	 */
	private void alCargarUsuarios(List<Usuario> usuarios) {
		adaptadorUsuario.cargar(usuarios);
	}

	/**
	 * Navega a la pantalla para añadir un nuevo usuario
	 */
	private void añadirUsuario() {
		NavController navController = ((MainActivity) getActivity()).getNavController();
		navController.navigate(R.id.navegacion_usuarios_a_añadir_usuario);
	}
}