package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.transfugaz.OfertaViajeAgregadoRepository;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

/**
 * ViewModel para el fragmento OfertasFragment
 */
public class OfertasFragmentViewModel extends ViewModel {

    private final OfertaViajeAgregadoRepository mRepositorio;
    private final LiveData<List<OfertaViaje>> mOfertasActuales;

    public OfertasFragmentViewModel(OfertaViajeAgregadoRepository repositorio) {
        mRepositorio = repositorio;
        mOfertasActuales = mRepositorio.getOfertasActuales();
    }

    public void solicitarRecuperarOfertasAPI() {
        mRepositorio.solicitarRecuperarOfertasAPI();
    }

    public LiveData<List<OfertaViaje>> getOfertasActuales() {
        return mOfertasActuales;
    }

    public LiveData<OfertaViaje.OfertaViajeDB> getOfertaBDPorId(Long id) {
        return mRepositorio.getOfertaBDPorId(id);
    }
}
