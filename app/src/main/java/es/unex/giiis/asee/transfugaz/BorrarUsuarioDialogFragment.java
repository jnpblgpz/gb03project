package es.unex.giiis.asee.transfugaz;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.vm.ListaUsuariosVM;

/**
 * Diálogo para confirmar el borrado de un viaje
 */
public class BorrarUsuarioDialogFragment extends DialogFragment {
	// Usuario a borrar
	private final Usuario usuario;
	// Necesario para actualizar gráficamente la lista de usuarios tras el borrado
	private AdaptadorUsuario adaptador;
	// Necesario para indicar qué elemento ha sido borrado
	private int posición;

	// ViewModel asociado a la lista de usuarios. Esta clase no puede tener su propio ViewModel ya que es un fragmento "detached".
	private ListaUsuariosVM listaUsuariosVM;

	public BorrarUsuarioDialogFragment(int posición, Usuario usuario, AdaptadorUsuario adaptador, ListaUsuariosVM listaUsuariosVM) {
		this.posición = posición;
		this.usuario = usuario;
		this.adaptador = adaptador;
		this.listaUsuariosVM = listaUsuariosVM;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity())
			// Mensaje de advertencia
			.setMessage(R.string.msg_borrarUsuario)
			// Confirmar el borrado
			.setPositiveButton(R.string.usuario_btn_borrar_ok, (dialog, which) -> {
				// Borrar el usuario de la BD local
				listaUsuariosVM.borrarUsuario(usuario.getId());
				MainActivity actividad = (MainActivity) getActivity();
				if (usuario.getId().equals(((AppTransFugaz) actividad.getApplication()).contenedorDep.usuarioConectado.getId())) {
					actividad.runOnUiThread(actividad::logout);
				}

				// Cerrar el diálogo
				dismiss();
			})
			// Cancelar el borrado
			.setNegativeButton(R.string.usuario_btn_borrar_cancelar, (dialog, which) -> dismiss())
			.create();
	}
}