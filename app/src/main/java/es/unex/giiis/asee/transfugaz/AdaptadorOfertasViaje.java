package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.vm.OfertasFragmentViewModel;

public class AdaptadorOfertasViaje extends RecyclerView.Adapter<AdaptadorOfertasViaje.ViewHolder> {

    // Soporte. Para conseguir el contexto de la app
    private MainActivity mainActivity;
    /*
     * ViewModel del fragmento contenedor (OfertasFragment). Usado para observar el LiveData asociado a la oferta con la que se
     * hace bind() para actualizarla gráficamente si sufre cambios.
     */
    private OfertasFragmentViewModel ofertasVM;

    private List<OfertaViaje> mListaOfertasViaje; // Colección de ofertas de viaje

    public AdaptadorOfertasViaje(MainActivity mainActivity, OfertasFragmentViewModel ofertasVM) {
        this.mainActivity = mainActivity;
        mListaOfertasViaje = new ArrayList<>();
        this.ofertasVM = ofertasVM;
    }

    /**
     * Reemplaza la colección de ofertas de este adaptador
     *
     * @param ofertas Colección de nuevas ofertas
     */
    public void reemplazarDatos(List<OfertaViaje> ofertas) {
        mListaOfertasViaje.clear(); // Vaciar la lista actual
        // Copiar las ofertas para impedir modificaciones externas de la fuente de datos del adaptador
        mListaOfertasViaje = new ArrayList<>(ofertas);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_ofertaviaje, parent, false);
        return new AdaptadorOfertasViaje.ViewHolder(v, mainActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Inicializar ViewHolder con los datos de la oferta almacenada en la posicion 'position'
        holder.bind(mListaOfertasViaje.get(position));
    }

    @Override
    public int getItemCount() {
        return mListaOfertasViaje.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Soporte. Para conseguir el contexto de la app
        private MainActivity mainActivity;

        // Vistas
        private View mItemView;
        private ImageView mImgEstado;
        private TextView mTvFechaCreacion;
        private TextView mTvCliente;

        public ViewHolder(@NonNull View itemView, MainActivity mainActivity) {
            super(itemView);
            this.mainActivity = mainActivity;
            this.mItemView = itemView;

            // Referencias a las vistas

            mImgEstado = itemView.findViewById(R.id.img_oferta_estado);
            mTvFechaCreacion = itemView.findViewById(R.id.tv_oferta_fechaCreacion);
            mTvCliente = itemView.findViewById(R.id.tv_oferta_cliente);
        }

        public void bind(final OfertaViaje ofertaViaje) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

            // Obtener el contenedor de dependencias de la aplicación
            AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) mainActivity.getApplication()).contenedorDep;
            // Observar el estado de la oferta de viaje para actualizarla gráficamente si sufre cambios
            ofertasVM.getOfertaBDPorId(ofertaViaje.getId()).observe(mainActivity, this::alCargarOfertaViajeBD);

            // Convertir la fecha de creación en una fecha más amigable
            mTvFechaCreacion.setText(sdf.format(new Date(ofertaViaje.getFechaCreacion() * 1000)));
            mTvCliente.setText(ofertaViaje.getCliente());

            // Al hacer clic en la vista navegar a los detalles de esa oferta
            mItemView.setOnClickListener(v -> mainActivity.runOnUiThread(() -> {
                NavController navController = mainActivity.getNavController();
                Bundle args = new Bundle();
                args.putSerializable(PaginacionOfertasFragment.NAV_ARG_ID_OFERTA,
                        ofertaViaje.getId());
                args.putSerializable(PaginacionOfertasFragment.NAV_ARG_LISTA_OFERTAS,
                        (Serializable) mListaOfertasViaje);
                navController.navigate(R.id.navegacion_ofertas_a_paginacionOfertas, args);
            }));
        }

        /**
         * Callback para manejar el estado de las ofertas de viaje
         *
         * @param ofertaViajeDB Representación local del estado de una oferta de viaje
         */
        private void alCargarOfertaViajeBD(OfertaViaje.OfertaViajeDB ofertaViajeDB) {
            if (ofertaViajeDB != null) {
                // Fijar icono y color de fondo según el estado de la oferta
                mainActivity.runOnUiThread(() -> {
                    switch (ofertaViajeDB.getEstado()) {
                        case ACEPTADA:
                            mImgEstado.setImageResource(R.drawable.icon_action_check_circle_24px);
                            mItemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(),
                                R.color.colorOfertaAceptada));
                            break;
                        case PENDIENTE:
                            mImgEstado.setImageResource(R.drawable.icon_action_cached_24px);
                            mItemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(),
                                android.R.color.background_light));
                            break;
                        case RECHAZADA:
                            mImgEstado.setImageResource(R.drawable.icon_navigation_close_24px);
                            mItemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(),
                                R.color.colorOfertaRechazada));
                            break;
                    }
                });
            }
        }
    }

}
