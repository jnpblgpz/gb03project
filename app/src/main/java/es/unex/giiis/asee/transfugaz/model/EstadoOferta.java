package es.unex.giiis.asee.transfugaz.model;

/**
 * Posibles estados en los que se puede encontrar una oferta de viaje: PENDIENTE de aceptación,
 * ACEPTADA para ser completada a través de un viaje y RECHAZADA
 */
public enum EstadoOferta {
    PENDIENTE, ACEPTADA, RECHAZADA
}
