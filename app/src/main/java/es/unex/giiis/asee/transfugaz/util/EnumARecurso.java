package es.unex.giiis.asee.transfugaz.util;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.R;
import es.unex.giiis.asee.transfugaz.model.EstadoMercancia;
import es.unex.giiis.asee.transfugaz.model.Rol;

/*
 * Clase utilizada para enlazar valores enumerados con IDs de recursos
 */
public class EnumARecurso {
	public static int getNombreRol(Rol rol) {
		switch (rol) {
			case ADMIN:
				return R.string.rol_admin;
			case TCA:
				return R.string.rol_tca;
			case CONDUCTOR:
				return R.string.rol_conductor;
			default:
				throw new IllegalArgumentException("Rol no vinculado a un recurso");
		}
	}

	public static int getColorRol(Rol rol) {
		switch (rol) {
			case ADMIN:
				return R.color.colorRolAdmin;
			case TCA:
				return R.color.colorRolTCA;
			case CONDUCTOR:
				return R.color.colorRolConductor;
			default:
				throw new IllegalArgumentException("Rol no vinculado a un recurso");
		}
	}

	public static int getEstadoMercancía(EstadoMercancia estado) {
		switch (estado) {
			case CORRECTA:
				return R.string.mercancía_correcta;
			case DAÑOS_LEVES:
				return R.string.mercancía_daños_leves;
			case DAÑOS_GRAVES:
				return R.string.mercancía_daños_graves;
			case PERDIDA:
				return R.string.mercancía_perdida;
			default:
				throw new IllegalArgumentException("Estado de mercancía no vinculado a un recurso");
		}
	}

	/**
	 * @param contexto Contexto de la aplicación
	 * @return Lista con los nombres de cada estado de mercancía almacenado en el tipo enumerado, usando los valores
	 * de las strings asociadas definidas en los recursos. La lista tiene el mismo orden que los valores del enum.
	 */
	public static List<String> getTodosEstadoMercancía(Context contexto) {
		List<String> res = new ArrayList<String>();

		for (EstadoMercancia estado : EstadoMercancia.values()) {
			res.add(contexto.getString(getEstadoMercancía(estado)));
		}

		return res;
	}
}
