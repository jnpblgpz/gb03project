package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;

public class AdaptadorViaje extends RecyclerView.Adapter<AdaptadorViaje.ViewHolder> {
	private List<Viaje> viajes = new ArrayList<Viaje>();
	/*
	 * Referencia a la actividad principal.
	 * Necesaria por el ViewHolder para acceder al usuario conectado, el hilo principal, etc.
	 */
	private MainActivity mainActivity;

	public AdaptadorViaje(MainActivity activity) {
		this.mainActivity = activity;
	}

	@Override
	public AdaptadorViaje.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_viaje, parent, false);
		return new ViewHolder(v, mainActivity);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bind(viajes.get(position));
	}

	@Override
	public int getItemCount() {
		return viajes.size();
	}

	public void cargar(List<Viaje> viajes){
		this.viajes.clear();
		this.viajes = viajes;
		notifyDataSetChanged();
	}

	public void añadir(Viaje viaje) {
		viajes.add(viaje);
		notifyDataSetChanged();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		private MainActivity actividad;

		// Elementos en un item de viaje
		private View viewViaje;
		private TextView id;
		private TextView empleado;
		private ImageView estado;

		public ViewHolder(View itemView, MainActivity actividad) {
			super(itemView);

			this.actividad = actividad;
			viewViaje = itemView;
			id = itemView.findViewById(R.id.tv_id_viaje);
			empleado = itemView.findViewById(R.id.tv_viaje_empleado);
			estado = itemView.findViewById(R.id.img_viaje_estado);
		}

		public void bind(final Viaje viaje) {
			id.setText(viaje.getId());

			Long idEmpleado = viaje.getIdConductor();
			if (idEmpleado == null) {
				// Viaje sin asignar
				empleado.setText(R.string.viaje_no_asignado);
			} else {
				// Obtener el nombre del conductor al que está asociado el viaje
				AppExecutors.getInstance().diskIO().execute(() -> {
					Usuario asignado = TransFugazDB.getInstance(actividad.getApplicationContext())
						.getDaoUsuario().getPorIdNLD(idEmpleado);
					actividad.runOnUiThread(() -> empleado.setText(asignado.getNombre()));
				});
			}

			if (viaje.isFinalizado()) {
				estado.setVisibility(View.VISIBLE);
				viewViaje.setBackgroundResource(R.color.colorViajeCompletado);
			} else {
				estado.setVisibility(View.INVISIBLE);
				viewViaje.setBackgroundResource(R.color.colorViajePendiente);
			}

			viewViaje.setOnClickListener(view -> actividad.runOnUiThread(() -> {
				NavController navController = actividad.getNavController();
				Bundle args = new Bundle();
				args.putSerializable(DetallesViajeFragment.ARG_VIAJE_DETALLE, viaje);
				navController.navigate(R.id.navegacion_transporte_a_detalleviaje, args);
			}));
		}
	}
}
