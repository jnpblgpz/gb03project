package es.unex.giiis.asee.transfugaz.api;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Fuente de datos para recuperar las ofertas de viaje disponibles a través de la API REST de
 * Transportes Fugaz SL ({@link OfertaViaje}
 */
public class OfertasViajeAPIDataSource {

    private static final String LOG_TAG = OfertasViajeAPIDataSource.class.getSimpleName();
    private static OfertasViajeAPIDataSource sInstance;

    private final Retrofit mRetrofit;

    // Mantiene las ofertas de viaje desde la última vez que se consultó la API REST empresarial
    private final MutableLiveData<List<OfertaViaje>> mOfertasDescargadas;

    private OfertasViajeAPIDataSource(Retrofit retrofit) {
        mRetrofit = retrofit;
        mOfertasDescargadas = new MutableLiveData<>();
    }

    /**
     * @return LiveData que contiene las ofertas de viaje desde la última actualización (más recientes)
     */
    public LiveData<List<OfertaViaje>> getListaOfertasActual() {
        return mOfertasDescargadas;
    }

    /**
     * Consulta la API REST de ofertas de viaje de Transportes Fugaz SL. Invocar este método
     * actualiza el LiveData que contiene la colección de ofertas de viaje disponibles
     */
    public void recuperarOfertasAPI() {
        Log.d(LOG_TAG, "Recuperando ofertas de viaje de la API REST ...");

        // Recuperar todas las ofertas de viaje disponibles y actualizar el LiveData
        AppExecutors.getInstance()
                .networkIO()
                .execute(new ListaOfertasLoader(mRetrofit, mOfertasDescargadas::postValue));
    }

    /**
     * @return Singleton de esta fuente de datos de ofertas de viaje en red
     */
    public synchronized static OfertasViajeAPIDataSource getInstance(Retrofit retrofit) {
        if (sInstance == null) {
            sInstance = new OfertasViajeAPIDataSource(retrofit);
            Log.d(LOG_TAG, "Nueva fuente de datos (OfertaViaje) de API REST creada");
        }

        return sInstance;
    }
}
