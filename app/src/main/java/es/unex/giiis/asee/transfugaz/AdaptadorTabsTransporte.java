package es.unex.giiis.asee.transfugaz;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

/**
 * Clase encargada de gestionar el cambio de pestaña en la página de transporte
 */
public class AdaptadorTabsTransporte extends FragmentStateAdapter {
	public AdaptadorTabsTransporte(Fragment fragment) {
		super(fragment);
	}

	@NonNull
	@Override
	public Fragment createFragment(int position) {
		switch (position) {
			case 0:
				return new OfertasFragment();
			case 1:
			default:
				return new ViajesFragment();
		}
	}

	@Override
	public int getItemCount() {
		return 2;
	}

}
