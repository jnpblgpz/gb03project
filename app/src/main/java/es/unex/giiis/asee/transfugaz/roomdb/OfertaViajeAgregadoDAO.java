package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

import static androidx.room.OnConflictStrategy.REPLACE;

/**
 * DAO para manipular el dominio agregado de ofertas de viaje en la BD Room local. El dominio agregado
 * incluye la representación de una oferta de viaje proporcionada por la API REST empresarial
 * ({@link OfertaViaje}), y su representación local en BD que almacena el estado de la oferta
 * ({@link OfertaViaje.OfertaViajeDB})
 */
@Dao
public interface OfertaViajeAgregadoDAO {

    // Representación de una oferta de viaje proporcionada por la API REST empresarial (OfertaViaje)

    @Query("SELECT * FROM OfertaViaje")
    LiveData<List<OfertaViaje>> getTodasOfertas();

    @Query("SELECT * FROM OfertaViaje WHERE id = :id")
    LiveData<OfertaViaje> getOfertaPorId(long id);

    @Query("SELECT count(*) FROM OfertaViaje")
    int cuantasOfertas();

    @Insert(onConflict = REPLACE)
    void insertarOfertasPorLotes(List<OfertaViaje> ofertasViaje);

    @Query("DELETE FROM OfertaViaje")
    void borrarTodasOfertas();

    // Representación local en BD que almacena el estado de la oferta (OfertaViajeBD)

    @Query("SELECT * FROM OfertaViajeDB")
    LiveData<List<OfertaViaje.OfertaViajeDB>> getTodasOfertasBD();

    @Query("SELECT * FROM OfertaViajeDB")
    List<OfertaViaje.OfertaViajeDB> getTodasOfertasBD_NLD();

    @Query("SELECT * FROM OfertaViajeDB WHERE id = :id")
    LiveData<OfertaViaje.OfertaViajeDB> getOfertaBDPorId(long id);

    @Query("SELECT * FROM OfertaViajeDB WHERE id = :id")
    OfertaViaje.OfertaViajeDB getOfertaBDPorId_NLD(long id);

    @Insert(onConflict = REPLACE)
    long insertarOfertaBD(OfertaViaje.OfertaViajeDB ofertaViaje);

    @Update
    int actualizarOfertaBD(OfertaViaje.OfertaViajeDB ofertaViaje);

    @Query("DELETE FROM OfertaViajeDB WHERE id = :id")
    void borrarOfertaBD(long id);

}
