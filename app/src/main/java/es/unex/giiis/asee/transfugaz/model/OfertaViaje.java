package es.unex.giiis.asee.transfugaz.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import es.unex.giiis.asee.transfugaz.roomdb.EstadoOfertaConverter;
import es.unex.giiis.asee.transfugaz.roomdb.MercanciaConverter;

/**
 * Modela una oferta de viaje recuperada de la API REST de Transportes Fugaz SL. Se almacena
 * localmente ligada a una política de caché.
 */
@Entity
public class OfertaViaje implements Serializable {

    @Ignore
    private static final long SerialVersionUID = 1L;

    @PrimaryKey
    private long id;
    private String cliente;
    private long fechaCreacion;
    private String origen;
    private long fechaEntrega;
    private String destino;
    @TypeConverters(MercanciaConverter.class)
    private List<String> mercancia;
    private float distancia;
    private float remuneracion;

    public OfertaViaje() {
        mercancia = new ArrayList<>();
    }

    public OfertaViaje(long id, String cliente, long fechaCreacion, String origen, long fechaEntrega, String destino,
                       List<String> mercancia, float distancia, float remuneracion) {
        this.id = id;
        this.cliente = cliente;
        this.fechaCreacion = fechaCreacion;
        this.origen = origen;
        this.fechaEntrega = fechaEntrega;
        this.destino = destino;
        this.mercancia = mercancia;
        this.distancia = distancia;
        this.remuneracion = remuneracion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public long getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(long fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public long getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(long fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public List<String> getMercancia() {
        return mercancia;
    }

    public void setMercancia(List<String> mercancia) {
        this.mercancia = mercancia;
    }

    public float getDistancia() {
        return distancia;
    }

    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }

    public float getRemuneracion() {
        return remuneracion;
    }

    public void setRemuneracion(float remuneracion) {
        this.remuneracion = remuneracion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfertaViaje that = (OfertaViaje) o;
        return id == that.id &&
                fechaCreacion == that.fechaCreacion &&
                fechaEntrega == that.fechaEntrega &&
                Float.compare(that.distancia, distancia) == 0 &&
                Float.compare(that.remuneracion, remuneracion) == 0 &&
                cliente.equals(that.cliente) &&
                origen.equals(that.origen) &&
                destino.equals(that.destino) &&
                mercancia.equals(that.mercancia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cliente, fechaCreacion, origen, fechaEntrega, destino, mercancia, distancia, remuneracion);
    }

    /**
     * Cómo se almacena una oferta en la BD local: identificador y estado. El identificador permite
     * asociarla con una oferta que viene de la API REST de ofertas
     */
    @Entity
    public static class OfertaViajeDB implements Serializable {

        @Ignore
        private static final long SerialVersionUID = 1L;

        @PrimaryKey
        private long id;
        @TypeConverters(EstadoOfertaConverter.class)
        private EstadoOferta estado;

        public OfertaViajeDB(long id, EstadoOferta estado) {
            this.id = id;
            this.estado = estado;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            OfertaViajeDB that = (OfertaViajeDB) o;
            return id == that.id &&
                estado == that.estado;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, estado);
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public EstadoOferta getEstado() {
            return estado;
        }

        public void setEstado(EstadoOferta estado) {
            this.estado = estado;
        }
    }
}
