package es.unex.giiis.asee.transfugaz.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;

@Database(entities = {Usuario.class, Viaje.class, OfertaViaje.class, OfertaViaje.OfertaViajeDB.class}, version = 10)
public abstract class TransFugazDB extends RoomDatabase {
	private static TransFugazDB instance;

    public static TransFugazDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, TransFugazDB.class, "TransFugaz.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

	public abstract UsuarioDAO getDaoUsuario();

	public abstract OfertaViajeAgregadoDAO getDaoOfertaViajeAgregado();

    public abstract ViajeDAO getDaoViaje();
}
