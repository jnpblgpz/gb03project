package es.unex.giiis.asee.transfugaz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.io.IOException;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoMercancia;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.util.EnumARecurso;
import es.unex.giiis.asee.transfugaz.vm.InformeViajeVM;

/**
 * Pantalla usada para informar de la finalización de un viaje por parte del conductor asignado
 */
public class InformeViajeFragment extends Fragment implements AdapterView.OnItemSelectedListener {
	public static final String ARG_VIAJE_A_INFORMAR = "viaje";
	private static final int IMG_NO_ADJUNTO = android.R.drawable.ic_menu_gallery;
	private static final int REQUEST_IMG = 1;

	private Viaje viaje;
	private EstadoMercancia estadoMercanciaSeleccionado;
	private Uri URIAdjunto = null;

	// Vistas
	private EditText et_distancia;
	private EditText et_tiempo;
	private EditText et_combustible;
	private Spinner spinnerEstadoMercancía;
	private EditText et_notas;
	private Button btn_adjuntar;
	private Button btn_borrar_adjunto;
	private ImageView iw_adjunto;

	private InformeViajeVM informeViajeVM;

	public InformeViajeFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		// Obtener el VM correspondiente a este fragmento
		AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
		informeViajeVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(InformeViajeVM.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_informe_finalizacion, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		List<String> nombresEstadoMercancía = EnumARecurso.getTodosEstadoMercancía(getContext());

		// Obtener el viaje sobre el que se realiza el informe de los parámetros de navegación
		viaje = (Viaje) getArguments().getSerializable(ARG_VIAJE_A_INFORMAR);

		// Elementos de la vista
		et_distancia = (EditText) view.findViewById(R.id.et_informe_distancia);
		et_tiempo = (EditText) view.findViewById(R.id.et_informe_tiempo);
		et_combustible = (EditText) view.findViewById(R.id.et_informe_combustible);
		spinnerEstadoMercancía = (Spinner) view.findViewById(R.id.spinner_informe_mercancía);
		et_notas = (EditText) view.findViewById(R.id.et_informe_notas);
		btn_adjuntar = (Button) view.findViewById(R.id.btn_adjuntar_documento);
		btn_borrar_adjunto = (Button) view.findViewById(R.id.btn_borrar_adjunto);
		iw_adjunto = (ImageView) view.findViewById(R.id.iw_adjunto);

		// Popular el spinner con los distintos estados de mercancía posibles
		ArrayAdapter<String> nombresEstadoMercancíaAdapter =
			new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, nombresEstadoMercancía);
		nombresEstadoMercancíaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerEstadoMercancía.setAdapter(nombresEstadoMercancíaAdapter);
		spinnerEstadoMercancía.setOnItemSelectedListener(this);

		// Listeners para los botones
		btn_adjuntar.setOnClickListener((view1) -> solicitarDocumento());
		btn_borrar_adjunto.setOnClickListener((view1) -> borrarAdjunto());
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		inflater.inflate(R.menu.menu_toolbar_informeviaje, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == R.id.acc_confirmar_informe_viaje) {
			// Marcar el viaje como resuelto y volver a la pantalla anterior
			confirmarInforme();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	/**
	 * Envía un intent implícito al sistema para que el usuario pueda seleccionar una imagen que adjuntar
	 */
	private void solicitarDocumento() {
		Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		intent.setType("image/*");

		// Confirmar que existe alguna actividad que puede gestionar el intent
		if (intent.resolveActivity(getContext().getPackageManager()) != null) {
			startActivityForResult(intent, REQUEST_IMG);
		}
	}

	/**
	 * Borra el documento adjunto seleccionado por el usuario, restaurando la imagen por defecto
	 */
	private void borrarAdjunto() {
		URIAdjunto = null;
		iw_adjunto.setImageResource(IMG_NO_ADJUNTO);
		btn_borrar_adjunto.setVisibility(View.INVISIBLE);
	}

	/**
	 * Confirma la creación del informe, cambia el estado del viaje a finalizado y vuelve a la pantalla anterior simulando
	 * una pulsación del botón back
	 */
	private void confirmarInforme() {
		new Thread(() -> {
			// Llamada síncrona
			informeViajeVM.setFinalizado(viaje, true);
			MainActivity mainActivity = (MainActivity) getActivity();
			mainActivity.runOnUiThread(() -> mainActivity.getNavController().popBackStack());
		}).start();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case REQUEST_IMG:
					URIAdjunto = data.getData();
					try {
						Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), URIAdjunto);
						iw_adjunto.setImageBitmap(bitmap);
						btn_borrar_adjunto.setVisibility(View.VISIBLE);
					} catch (IOException e) {
						URIAdjunto = null;
					}
					break;
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		// Actualizar el estado de mercancía seleccionado en el Spinner
		estadoMercanciaSeleccionado = EstadoMercancia.values()[position];
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}
}