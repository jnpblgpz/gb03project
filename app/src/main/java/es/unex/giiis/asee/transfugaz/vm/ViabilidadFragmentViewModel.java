package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.OfertaViajeAgregadoRepository;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Viaje;

public class ViabilidadFragmentViewModel extends ViewModel {

    private ViajeRepository repositorioViajes;
    private final OfertaViajeAgregadoRepository repositorioOfertas;
    private final MutableLiveData<Long> mFiltroIdentificadorOferta;

    public ViabilidadFragmentViewModel(OfertaViajeAgregadoRepository ofertasAgregadoRepositorio, ViajeRepository repositorioViajes) {
        repositorioOfertas = ofertasAgregadoRepositorio;
        this.repositorioViajes = repositorioViajes;
        mFiltroIdentificadorOferta = new MutableLiveData<>();
    }

    public void setIdentificadorOferta(Long idOferta) {
        mFiltroIdentificadorOferta.setValue(idOferta);
    }

    public void cambiarEstadoOfertaBD(EstadoOferta nuevoEstado) {
        repositorioOfertas
                .cambiarEstadoOfertaBD(mFiltroIdentificadorOferta.getValue(), nuevoEstado);
    }

    public LiveData<OfertaViaje> getOfertaViaje() {
        return Transformations.switchMap(mFiltroIdentificadorOferta,
                repositorioOfertas::getOfertaPorId);
    }

    /**
     * Crea un nuevo viaje en la base de datos. Dado que la pantalla de viabilidad debe esperar a que esta inserción finalice antes
     * de navegar a la pantalla de detalles de viaje, este método no crea otro hilo para su ejecución.
     * Por tanto, la vista es responsable del control de concurrencia.
     * @param viaje El viaje a insertar en la base de datos
     */
    public void crearViaje(Viaje viaje) {
        repositorioViajes.insertar(viaje);
    }
}
