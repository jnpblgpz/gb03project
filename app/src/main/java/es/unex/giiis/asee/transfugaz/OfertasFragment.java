package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.vm.OfertasFragmentViewModel;

public class OfertasFragment extends Fragment {

    private RecyclerView mRvOfertas;
    private RecyclerView.LayoutManager mLayoutManager;
    private AdaptadorOfertasViaje mAdaptadorOfertas;

    public OfertasFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ofertas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        OfertasFragmentViewModel viewModel =
                new ViewModelProvider(this, contenedorDep.appVMFactory).get(OfertasFragmentViewModel.class);

        // OBTENER REFERENCIAS A LAS VISTAS

        mRvOfertas = view.findViewById(R.id.rv_ofertasviaje);

        // Configurar la RecyclerView

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRvOfertas.setLayoutManager(mLayoutManager);
        mAdaptadorOfertas = new AdaptadorOfertasViaje((MainActivity) getActivity(), viewModel);
        mRvOfertas.setAdapter(mAdaptadorOfertas);
        // Observar la colección de ofertas actuales y recargarlas en el adaptador cuando cambien
        viewModel.getOfertasActuales().observe(getViewLifecycleOwner(), this::alCargarOfertasViaje);
        // Refrescar la fuente de datos
        viewModel.solicitarRecuperarOfertasAPI();
    }

    /**
     * Callback para manejar la carga de nuevas ofertas de viaje en el adaptador de la RV
     *
     * @param ofertasViaje Nueva colección de ofertas de viaje
     */
    private void alCargarOfertasViaje(List<OfertaViaje> ofertasViaje) {
        if (ofertasViaje != null && ofertasViaje.size() > 0) {
            mAdaptadorOfertas.reemplazarDatos(ofertasViaje);
        }
    }

}