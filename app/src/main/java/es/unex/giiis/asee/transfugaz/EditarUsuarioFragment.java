package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.vm.EditarUsuarioVM;

public class EditarUsuarioFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private Usuario mUsuarioEditado;
    private Rol mRolSeleccionado;
    private EditarUsuarioVM editarUsuarioVM;

    // Vistas
    private EditText mEt_nombreUsuario;
    private TextInputEditText mTiEt_password;
    private Spinner mSpinnerRol;

    public EditarUsuarioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Obtener el VM correspondiente a este fragmento
        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        editarUsuarioVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(EditarUsuarioVM.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_editar_usuario, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<String> nombresRol = Rol.nombresRol();

        // RECUPERAR EL USUARIO QUE ESTÁ SIENDO EDITADO
        mUsuarioEditado = (Usuario) getArguments().getSerializable("usuario");

        // Cambiar el título de la ActionBar
        ((MainActivity) getActivity()).actionBarCambiarTitulo(
                getResources().getString(R.string.pantalla_editar_usuario) + " - " +
                        mUsuarioEditado.getNombre());

        // INICIALIZAR REFERENCIAS A VISTAS

        mEt_nombreUsuario = (EditText) view.findViewById(R.id.et_nombreUsuario);
        mTiEt_password = (TextInputEditText) view.findViewById(R.id.tiet_pwd);
        mSpinnerRol = (Spinner) view.findViewById(R.id.spinner_rol);

        // CONFIGURAR LAS VISTAS QUE REQUIERAN CONFIGURACIÓN ADICIONAL

        // Popular el spinner con los distintos roles
        ArrayAdapter<String> nombresRolAdapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, nombresRol);
        nombresRolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerRol.setAdapter(nombresRolAdapter);
        mSpinnerRol.setOnItemSelectedListener(this);

        // RELLENAR LAS VISTAS CON LA INFO PREVIA DEL USUARIO

        mEt_nombreUsuario.setText(mUsuarioEditado.getNombre());
        // La contraseña no se rellena por seguridad
        mSpinnerRol.setSelection(nombresRol.indexOf(mUsuarioEditado.getRol().name()));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar_editarusuario, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.acc_confirmar_edicion_usuario) {
            // Aplicar los cambios sobre el usuario editado y volver a la lista de usuarios
            confirmarCambiosUsuario();
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Guardar los nuevos datos del usuario siendo editado en la capa de persistencia y emula pulsar
     * el botón 'Back'
     */
    private void confirmarCambiosUsuario() {

        mUsuarioEditado.setNombre(mEt_nombreUsuario.getText().toString());
        String contraseñaElegida = mTiEt_password.getText().toString();
        // Si la contraseña se deja en blanco, no se modifica
        if (!contraseñaElegida.equals("")) {
            mUsuarioEditado.setContraseña(contraseñaElegida);
        }
        mUsuarioEditado.setRol(mRolSeleccionado);

        // Persistir el cambio con la base de datos Room
        editarUsuarioVM.editarUsuario(mUsuarioEditado);
        // Botón 'Back'
        ((MainActivity) getActivity()).getNavController().popBackStack();

        // Si el usuario siendo editado es el usuario logeado, actualizarlo en MainActivity
        if (((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado.getId().equals(mUsuarioEditado.getId())) {
            ((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado = mUsuarioEditado;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Actualizar el rol seleccionado en el Spinner
        mRolSeleccionado = Rol.valueOf(parent.getItemAtPosition(position).toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}