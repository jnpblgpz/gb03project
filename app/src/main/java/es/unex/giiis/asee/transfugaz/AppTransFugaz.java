package es.unex.giiis.asee.transfugaz;

import android.app.Application;
import android.content.Context;

import es.unex.giiis.asee.transfugaz.api.ConfiguracionAPI;
import es.unex.giiis.asee.transfugaz.api.OfertasViajeAPIDataSource;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.vm.AppViewModelFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Para registrar el contenedor de aplicación como parte del objeto 'Application'
 */
public class AppTransFugaz extends Application {

	// El objeto 'Application' almacena el contenedor de dependencias
	public AppContainer contenedorDep;

	@Override
	public void onCreate() {
		super.onCreate();
		contenedorDep = new AppContainer(this);
	}

	/**
	 * Contenedor de aplicación para resolver las principales dependencias de la App
	 */
	public static class AppContainer {

		private TransFugazDB mBDRoomLocal;
		// Para consultar la API REST empresarial de Transportes Fugaz SL
		private Retrofit mRetrofitAPIEmpresarial;
		private OfertasViajeAPIDataSource mOfertasRESTDataSource;
		public AppViewModelFactory appVMFactory;
		/*
		 * Usuario conectado actualmente en el sistema. null si no se ha iniciado sesión todavía.
		 * No es un LiveData ya que al cerrar sesión hay que fijarlo a null manualmente, cosa que un LiveData no permite.
		 */
		public Usuario usuarioConectado;

		public AppContainer(Context context) {
			mBDRoomLocal = TransFugazDB.getInstance(context);
			mRetrofitAPIEmpresarial = new Retrofit.Builder()
				.baseUrl(ConfiguracionAPI.TF_OFERTAS_RESTAPI_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
			mOfertasRESTDataSource = OfertasViajeAPIDataSource.getInstance(mRetrofitAPIEmpresarial);

			appVMFactory = new AppViewModelFactory(mBDRoomLocal, mOfertasRESTDataSource);

			usuarioConectado = null;
		}

	}
}
