package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.room.TypeConverter;

/**
 * Conversor de tipo Room para traducir un valor entero en un valor booleano y viceversa. Un valor
 * entero es equivalente a 'false' si es igual a 0, y 'true' en cualquier otro caso.
 */
public class BooleanConverter {

    @TypeConverter
    public static boolean aBooleano(int valor) {
        return valor != 0;
    }

    @TypeConverter
    public static int aValor(boolean booleano) {
        return booleano ? 1 : 0;
    }

}
