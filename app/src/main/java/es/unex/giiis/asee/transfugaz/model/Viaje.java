package es.unex.giiis.asee.transfugaz.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.unex.giiis.asee.transfugaz.roomdb.BooleanConverter;
import es.unex.giiis.asee.transfugaz.roomdb.CalendarConverter;
import es.unex.giiis.asee.transfugaz.roomdb.MercanciaConverter;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.SET_NULL;

/**
 * Modela un viaje derivado de una oferta de viaje aceptada y asignado a un conductor
 */
@Entity(foreignKeys = @ForeignKey(entity = Usuario.class,
        parentColumns = "id",
        childColumns = "idConductor",
        onDelete = SET_NULL,
        onUpdate = CASCADE))
public class Viaje implements Serializable {

    @Ignore
    private static final long SerialVersionUID = 1L;

    @NotNull
    @PrimaryKey
    private String id;
    private Long idConductor; // Debe ser un tipo nullable
    private String origen;
    private String destino;
    @TypeConverters(CalendarConverter.class)
    private Calendar fechaDescarga;
    @TypeConverters(MercanciaConverter.class)
    private List<String> mercancia;
    private float distancia;
    @TypeConverters(BooleanConverter.class)
    private boolean finalizado;

    // Formato de fecha reducido
    @Ignore
    private final SimpleDateFormat dateShortFormat =
            new SimpleDateFormat("dd/MM/yy HH:mm", Locale.getDefault());
    // Formato de fecha extendido
    @Ignore
    private final SimpleDateFormat dateExtendedFormat =
            new SimpleDateFormat("EEEE dd MMMM yyyy, HH:mm", Locale.getDefault());

    public Viaje() {
    }

    public Viaje(@NotNull String id, Long idConductor, String origen, String destino, Calendar fechaDescarga, List<String> mercancia, float distancia, boolean finalizado) {
        this.id = id;
        this.idConductor = idConductor;
        this.origen = origen;
        this.destino = destino;
        this.fechaDescarga = fechaDescarga;
        this.mercancia = mercancia;
        this.distancia = distancia;
        this.finalizado = finalizado;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    public Long getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(Long idConductor) {
        this.idConductor = idConductor;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Calendar getFechaDescarga() {
        return fechaDescarga;
    }

    public void setFechaDescarga(Calendar fechaDescarga) {
        this.fechaDescarga = fechaDescarga;
    }

    public List<String> getMercancia() {
        return mercancia;
    }

    public void setMercancia(List<String> mercancia) {
        this.mercancia = mercancia;
    }

    public float getDistancia() {
        return distancia;
    }

    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    public String fechaDescargaFormatoExtendido() {
        return dateExtendedFormat.format(this.getFechaDescarga().getTime());
    }

    public String fechaDescargaFormatoReducido() {
        return dateShortFormat.format(this.getFechaDescarga().getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Viaje viaje = (Viaje) o;
        return Float.compare(viaje.distancia, distancia) == 0 &&
                finalizado == viaje.finalizado &&
                id.equals(viaje.id) &&
                idConductor.equals(viaje.idConductor) &&
                origen.equals(viaje.origen) &&
                destino.equals(viaje.destino) &&
                mercancia.equals(viaje.mercancia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idConductor, origen, destino, fechaDescarga, mercancia, distancia, finalizado);
    }
}
