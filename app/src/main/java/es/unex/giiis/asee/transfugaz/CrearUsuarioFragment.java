package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.vm.CrearUsuarioVM;

public class CrearUsuarioFragment extends Fragment implements AdapterView.OnItemSelectedListener {

	private Rol rolSeleccionado = Rol.CONDUCTOR;
	private CrearUsuarioVM crearUsuarioVM;
	// Vistas
	private EditText et_nombreUsuario;
	private TextInputEditText et_password;
	private Spinner spinnerRol;

	public CrearUsuarioFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		// Obtener el VM correspondiente a este fragmento
		AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
		crearUsuarioVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(CrearUsuarioVM.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Reutilizamos el layout de edición de usuario ya que los campos son los mismos
		return inflater.inflate(R.layout.fragment_editar_usuario, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		List<String> nombresRol = Rol.nombresRol();

		// Inicializar referencias a vistas
		et_nombreUsuario = (EditText) view.findViewById(R.id.et_nombreUsuario);
		et_password = (TextInputEditText) view.findViewById(R.id.tiet_pwd);
		spinnerRol = (Spinner) view.findViewById(R.id.spinner_rol);

		// Popular el spinner con los distintos roles
		ArrayAdapter<String> nombresRolAdapter =
			new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, nombresRol);
		nombresRolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerRol.setAdapter(nombresRolAdapter);
		spinnerRol.setOnItemSelectedListener(this);
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		// Reutilizamos las opciones del menú de edición de usuario
		inflater.inflate(R.menu.menu_toolbar_editarusuario, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == R.id.acc_confirmar_edicion_usuario) {
			// Crear el usuario con los valores especificados y volver a la lista de usuarios
			crearUsuario();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	/**
	 * Crea un nuevo usuario en la base de datos con la información proporcionada y simula una pulsación de 'back'
	 */
	private void crearUsuario() {
		Usuario nuevoUsuario = new Usuario(
			null,
			et_nombreUsuario.getText().toString(),
			et_password.getText().toString(),
			rolSeleccionado
		);

		// Almacenar en la BD
		UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();
		crearUsuarioVM.insertarUsuario(nuevoUsuario);
		// Simular pulsación del botón back
		MainActivity mainActivity = (MainActivity) getActivity();
		mainActivity.runOnUiThread(() -> mainActivity.getNavController().popBackStack());
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		// Actualizar el rol seleccionado en el Spinner
		rolSeleccionado = Rol.valueOf(parent.getItemAtPosition(position).toString());
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}
}