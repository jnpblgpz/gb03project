package es.unex.giiis.asee.transfugaz.model;

// Usado en los informes de finalización para indicar en qué estado se encuentra la mercancía en la entrega
public enum EstadoMercancia {
	CORRECTA,
	DAÑOS_LEVES,
	DAÑOS_GRAVES,
	PERDIDA;
}
