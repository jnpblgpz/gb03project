package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;

public class MainActivity extends AppCompatActivity {

    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentoPrincipal);
        navController = navHostFragment.getNavController();

        // Toolbar
        // Declarar los fragmentos que corresponden a pantallas de nivel superior (no tendrán botón up)
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.loginFragment, R.id.transporteFragment, R.id.vacacionesFragment, R.id.usuariosFragment).build();
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);

        // Navegación inferior
        final BottomNavigationView navInferior = findViewById(R.id.nav_inferior);
        NavigationUI.setupWithNavController(navInferior, navController);

        // Gestión de cambio de pantalla
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination,
                                             @Nullable Bundle arguments) {
                // Ocultar la toolbar y la navegación inferior en la página de login
                if (destination.getId() == R.id.loginFragment) {
                    toolbar.setVisibility(View.GONE);
                    navInferior.setVisibility(View.GONE);
                } else {
                    toolbar.setVisibility(View.VISIBLE);

                    Usuario usuarioConectado = ((AppTransFugaz) getApplication()).contenedorDep.usuarioConectado;

                    // Ocultar el botón de "usuarios" de la navegación inferior si el usuario actual no es admin
                    MenuItem opcionUsuarios = navInferior.getMenu().findItem(R.id.usuariosFragment);
                    opcionUsuarios.setVisible(usuarioConectado != null && usuarioConectado.getRol().mayorIgual(Rol.ADMIN));
                    navInferior.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    /**
     * Cambia el título de la ActionBar
     *
     * @param nuevoTitulo Nuevo título
     */
    public void actionBarCambiarTitulo(String nuevoTitulo) {
        getSupportActionBar().setTitle(nuevoTitulo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    /*
     * Gestión de pulsaciones a elementos de menú comunes
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*
             * Navegación inferior
             * Como los botones tienen la misma id que los fragmentos de destino, lo gestiona NavigationUI
             */
            case R.id.transporteFragment:
            case R.id.vacacionesFragment:
            case R.id.usuariosFragment:
                if (NavigationUI.onNavDestinationSelected(item, navController)) {
                    return true;
                } else {
                    return super.onOptionsItemSelected(item);
                }
            case R.id.logoutAction:
                // Cerrar sesión
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("usuario", ((AppTransFugaz) getApplication()).contenedorDep.usuarioConectado);
    }

    /**
     * Cierra la sesión del usuario actual y lo redirige a la pantalla de login. Como causa una redirección, debe
     * ejecutarse siempre en el hilo principal
     */
    public void logout() {
        ((AppTransFugaz) getApplication()).contenedorDep.usuarioConectado = null;
        navController.navigate(R.id.navegacion_a_login);
    }

    public NavController getNavController() {
        return navController;
    }
}