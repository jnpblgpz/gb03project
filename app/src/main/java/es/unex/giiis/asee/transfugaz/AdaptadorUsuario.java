package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.util.EnumARecurso;
import es.unex.giiis.asee.transfugaz.vm.ListaUsuariosVM;

public class AdaptadorUsuario extends RecyclerView.Adapter<AdaptadorUsuario.ViewHolder> {
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	/*
	 * Referencia a la actividad principal.
	 * Necesaria por el ViewHolder para acceder al usuario conectado, el hilo principal, etc.
	 */
	private MainActivity mainActivity;
	/*
	 * Fragmento en el que están contenidos los elementos que crea este adaptador.
	 * Lo necesita el diálogo de confirmación de borrado
	 */
	private Fragment fragmentoContenedor;

	public AdaptadorUsuario(MainActivity activity, Fragment fragmentoContenedor) {
		this.mainActivity = activity;
		this.fragmentoContenedor = fragmentoContenedor;
	}

	@Override
	public AdaptadorUsuario.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario, parent, false);
		return new ViewHolder(v, mainActivity);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bind(usuarios.get(position), fragmentoContenedor, this);
	}

	@Override
	public int getItemCount() {
		return usuarios.size();
	}

	public void cargar(List<Usuario> usuarios){
		this.usuarios.clear();
		this.usuarios = usuarios;
		notifyDataSetChanged();
	}

	public void añadir(Usuario usuario) {
		usuarios.add(usuario);
		notifyDataSetChanged();
	}

	public void borrar(int posición) {
		usuarios.remove(posición);
		notifyItemRemoved(posición);
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		private MainActivity actividad;

		// Elementos en un item de usuario
		private TextView nombre;
		private TextView rol;
		private ImageView editar;
		private ImageView borrar;

		public ViewHolder(View itemView, MainActivity actividad) {
			super(itemView);

			this.actividad = actividad;
			nombre = itemView.findViewById(R.id.nombreUsuario);
			rol = itemView.findViewById(R.id.rolUsuario);
			editar = itemView.findViewById(R.id.editarUsuario);
			borrar = itemView.findViewById(R.id.borrarUsuario);
		}

		public void bind(final Usuario usuario, Fragment fragmentoContenedor, AdaptadorUsuario adaptador) {
			nombre.setText(usuario.getNombre());
			rol.setText(EnumARecurso.getNombreRol(usuario.getRol()));
			rol.setBackgroundResource(EnumARecurso.getColorRol(usuario.getRol()));
			editar.setOnClickListener(view -> actividad.runOnUiThread(() -> {
				NavController navController = actividad.getNavController();
				Bundle args = new Bundle();
				args.putSerializable("usuario", usuario);
				navController.navigate(R.id.navegacion_usuarios_a_editar_usuario, args);
			}));

			borrar.setOnClickListener(view -> actividad.runOnUiThread(() -> {
				// Mostrar diálogo para confirmar el borrado. Se necesita el ViewModel del fragmento de la lista de usuarios
				AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) actividad.getApplication()).contenedorDep;
				ListaUsuariosVM listaUsuariosVM =
					new ViewModelProvider(fragmentoContenedor, contenedorDep.appVMFactory).get(ListaUsuariosVM.class);

				new BorrarUsuarioDialogFragment(getAdapterPosition(), usuario, adaptador, listaUsuariosVM)
					.show(fragmentoContenedor.getParentFragmentManager(), "dialogoConfirmacionBorrarUsuario");
			}));
		}
	}
}
