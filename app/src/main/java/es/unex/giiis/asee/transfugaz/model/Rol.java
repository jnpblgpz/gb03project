package es.unex.giiis.asee.transfugaz.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Roles que puede tener un usuario en el sistema. Deben almacenarse de mayor a menor rango
 */
public enum Rol {
    ADMIN,
    TCA,
    CONDUCTOR;

    /**
     * @return Lista con los nombres de cada rol almacenado en el tipo enumerado
     */
    public static List<String> nombresRol() {
        return Stream.of(Rol.values()).map(Enum::toString).collect(Collectors.toList());
    }

    /**
     * Comprueba si el rol actual es mayor o igual a otro rol dado
     * @param otro El otro rol con el que realizar la comparación
     * @return True si el rol actual es mayor o igual que 'otro' en la cadena de roles
     */
    public boolean mayorIgual(Rol otro) {
        return this.ordinal() <= otro.ordinal();
    }
}
