package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;
import es.unex.giiis.asee.transfugaz.vm.EditarViajeVM;

/**
 * Pantalla para editar los campos de un viaje
 */
public class EditarViajeFragment extends Fragment implements AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    // Clave para recuperar de los argumentos de navegación el viaje que está siendo editado
    public static final String ARG_VIAJE_EDITADO = "viajeEdiado";

    private EditarViajeVM editarViajeVM;

    private Viaje mViajeEditado; // Viaje que está siendo editado en esta pantalla
    private String mNombreConductorAsignado; // Nombre del conductor elegido en el Spinner
    private RecyclerView.LayoutManager mRvLayoutManager;
    private final AdaptadorMercancia mAdaptadorMercancia;
    // Vistas
    Spinner mSpinnnerConductor;
    EditText mEtOrigen;
    EditText mEtDestino;
    TextView mTvFechatiempo;
    Button mBtnCambiarFecha;
    Button mBtnCambiarHora;
    RecyclerView mRvMercancia;
    EditText mEtAgregarMercancia;
    Button mBtnAgregarMercancia;
    EditText mEtDistancia;
    CheckBox mCkbxFinalizado;

    public EditarViajeFragment() {
        mAdaptadorMercancia = new AdaptadorMercancia(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editar_viaje, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();
        // Obtener el VM correspondiente a este fragmento
        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        editarViajeVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(EditarViajeVM.class);

        // RECUPERAR EL VIAJE QUE DEBE SER EDITADO DE LOS ARGUMENTOS

        Bundle arguments = getArguments();
        if (arguments != null)
            mViajeEditado = ((Viaje) arguments.getSerializable(ARG_VIAJE_EDITADO));
        else
            throw new IllegalArgumentException();

        // Cambiar el título de la ActionBar al ID del viaje
        ((MainActivity) getActivity()).actionBarCambiarTitulo(
                getResources().getString(R.string.pantalla_editar_viaje) + " - " +
                        mViajeEditado.getId());

        // INICIALIZAR REFERENCIAS A VISTAS

        mSpinnnerConductor = view.findViewById(R.id.spinner_viaje_conductor);
        mEtOrigen = view.findViewById(R.id.et_viaje_origen);
        mEtDestino = view.findViewById(R.id.et_viaje_destino);
        mTvFechatiempo = view.findViewById(R.id.tv_viaje_fechatiempo);
        mBtnCambiarFecha = view.findViewById(R.id.btn_viaje_cambiarfecha);
        mBtnCambiarHora = view.findViewById(R.id.btn_viaje_cambiarhora);
        mRvMercancia = view.findViewById(R.id.rv_viaje_mercancia);
        mEtAgregarMercancia = view.findViewById(R.id.et_viaje_agragarMercancia);
        mBtnAgregarMercancia = view.findViewById(R.id.btn_viaje_agregarMercancia);
        mEtDistancia = view.findViewById(R.id.et_viaje_distancia);
        mCkbxFinalizado = view.findViewById(R.id.ckbx_viaje_finalizado);

        // CONFIGURAR VISTAS

        AppExecutors.getInstance().diskIO().execute(() -> {
            // Lista con los nombres de todos los posibles conductores (roles CONDUCTOR y TCA)
            List<String> nombresConductores = new ArrayList<>();
            nombresConductores.addAll(daoUsuario.getNombresPorRol(Rol.CONDUCTOR.ordinal()));
            nombresConductores.addAll(daoUsuario.getNombresPorRol(Rol.TCA.ordinal()));

            // Spinner para seleccionar el nombre del conductor asignado
            ArrayAdapter<String> usuarioArrayAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item,
                    nombresConductores);
            usuarioArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnnerConductor.setAdapter(usuarioArrayAdapter);
            mSpinnnerConductor.setOnItemSelectedListener(this);

            // Presentar seleccionado el rol del usuario siendo editado
            Long idConductor = mViajeEditado.getIdConductor();
            String nombreConductorActual = idConductor == null ?
                    getResources().getString(R.string.viaje_no_asignado) :
                    daoUsuario.getPorIdNLD(idConductor).getNombre();
            mSpinnnerConductor.setSelection(nombresConductores.indexOf(nombreConductorActual));
        });

        // El botón de cambiar fecha abre un diálogo estilo DatePicker
        mBtnCambiarFecha.setOnClickListener(v -> {
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    EditarViajeFragment.this,
                    mViajeEditado.getFechaDescarga().get(Calendar.YEAR),
                    mViajeEditado.getFechaDescarga().get(Calendar.MONTH),
                    mViajeEditado.getFechaDescarga().get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getParentFragmentManager(), "Datepickerdialog");
        });

        // El botón de cambiar hora abre un diálogo estilo TimePicker
        mBtnCambiarHora.setOnClickListener(v -> {
            TimePickerDialog tpd = TimePickerDialog.newInstance(EditarViajeFragment.this,
                    mViajeEditado.getFechaDescarga().get(Calendar.HOUR_OF_DAY),
                    mViajeEditado.getFechaDescarga().get(Calendar.MINUTE),
                    mViajeEditado.getFechaDescarga().get(Calendar.SECOND),
                    true);
            tpd.show(getParentFragmentManager(), "Timepickerdialog");
        });

        // Preparar la RecyclerView y el adaptador para la lista editable de ítems en la mercancía
        mRvLayoutManager = new LinearLayoutManager(getActivity());
        mRvMercancia.setLayoutManager(mRvLayoutManager);
        mAdaptadorMercancia.cargarItems(mViajeEditado.getMercancia());
        mRvMercancia.setAdapter(mAdaptadorMercancia);

        // Añadir al adaptador de mercancía un nuevo ítem al pulsar el botón 'Añadir'
        mBtnAgregarMercancia.setOnClickListener(v -> {
            String nuevoItem = mEtAgregarMercancia.getText().toString();

            if (!nuevoItem.trim().isEmpty()) // Solo añadir si se ha introducido algún nuevo ítem
                mAdaptadorMercancia.agregarItem(nuevoItem);

            mEtAgregarMercancia.setText(""); // Vaciar el campo para admitir un nuevo ítem
        });

        // ASIGNAR VALORES INICIALES A LAS VISTAS

        mEtOrigen.setText(mViajeEditado.getOrigen());
        mEtDestino.setText(mViajeEditado.getDestino());
        mTvFechatiempo.setText(mViajeEditado.fechaDescargaFormatoReducido());
        mEtDistancia.setText(Float.toString(mViajeEditado.getDistancia()));
        mCkbxFinalizado.setChecked(mViajeEditado.isFinalizado());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar_editarviaje, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.acc_confirmar_edicion_viaje) {
            confirmarCambios();
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Persistir los cambios en la BD Room
     */
    private void confirmarCambios() {
        ViajeDAO daoViaje = TransFugazDB.getInstance(getContext()).getDaoViaje();
        UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();

        // Persistir los cambios en la BD Room

        AppExecutors.getInstance().diskIO().execute(() -> {
            // Actualizar los atributos del viaje que no hayan sido ya actualizados
            Long idNuevoConductor = daoUsuario.getPorNombreNLD(mNombreConductorAsignado).getId();
            mViajeEditado.setIdConductor(idNuevoConductor);

            mViajeEditado.setOrigen(mEtOrigen.getText().toString());
            mViajeEditado.setDestino(mEtDestino.getText().toString());
            mViajeEditado.setMercancia(mAdaptadorMercancia.consultarMercancia());
            mViajeEditado.setDistancia(Float.parseFloat(mEtDistancia.getText().toString()));
            mViajeEditado.setFinalizado(mCkbxFinalizado.isChecked());

            // Llamada síncrona
            editarViajeVM.editarViaje(mViajeEditado);
            // Navegar de vuelta al detalle del viaje. Equivalente a pulsar el botón 'Back'
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.runOnUiThread(() -> mainActivity.getNavController().popBackStack());
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Actualizar el conductor asignado
        mNombreConductorAsignado = ((String) parent.getItemAtPosition(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        // Actualizar la fecha de descarga en la instancia de viaje
        mViajeEditado.getFechaDescarga().set(Calendar.YEAR, year);
        mViajeEditado.getFechaDescarga().set(Calendar.MONTH, monthOfYear);
        mViajeEditado.getFechaDescarga().set(Calendar.DAY_OF_MONTH, dayOfMonth);

        // Actualizar la fecha de descarga en la UI
        mTvFechatiempo.setText(mViajeEditado.fechaDescargaFormatoReducido());
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        // Actualizar la hora de la fecha de descarga en la instancia de viaje
        mViajeEditado.getFechaDescarga().set(Calendar.HOUR_OF_DAY, hourOfDay);
        mViajeEditado.getFechaDescarga().set(Calendar.MINUTE, minute);
        mViajeEditado.getFechaDescarga().set(Calendar.SECOND, second);

        // Actualizar la hora de descarga en la UI
        mTvFechatiempo.setText(mViajeEditado.fechaDescargaFormatoReducido());
    }
}