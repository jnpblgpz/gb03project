package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.model.Usuario;

public class EditarUsuarioVM extends ViewModel {
	private UsuarioRepository repositorio;

	public EditarUsuarioVM(UsuarioRepository repositorio) {
		this.repositorio = repositorio;
	}

	public void editarUsuario(Usuario usuario) {
		AppExecutors.getInstance()
			.diskIO()
			.execute(() -> {
				repositorio.actualizar(usuario);
			});
	}
}
