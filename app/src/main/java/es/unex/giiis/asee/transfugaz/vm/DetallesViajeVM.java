package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;

public class DetallesViajeVM extends ViewModel {
	private ViajeRepository repositorio;

	public DetallesViajeVM(ViajeRepository repositorio) {
		this.repositorio = repositorio;
	}

	public void borrarViaje(String id) {
		AppExecutors.getInstance()
			.diskIO()
			.execute(() -> {
				repositorio.borrar(id);
			});
	}
}
