package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.OfertaViajeAgregadoRepository;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

public class DetallesOfertaFragmentViewModel extends ViewModel {

    private final OfertaViajeAgregadoRepository mRepositorio;
    private final MutableLiveData<Long> mFiltroIdentificadorOferta;

    public DetallesOfertaFragmentViewModel(OfertaViajeAgregadoRepository repositorio) {
        mRepositorio = repositorio;
        mFiltroIdentificadorOferta = new MutableLiveData<>();
    }

    public void setIdentificadorOferta(Long idOferta) {
        mFiltroIdentificadorOferta.setValue(idOferta);
    }

    public LiveData<OfertaViaje> getOfertaViaje() {
        return Transformations.switchMap(mFiltroIdentificadorOferta, mRepositorio::getOfertaPorId);
    }

    public LiveData<OfertaViaje.OfertaViajeDB> getOfertaViajeBD() {
        return Transformations.switchMap(mFiltroIdentificadorOferta, mRepositorio::getOfertaBDPorId);
    }
}
