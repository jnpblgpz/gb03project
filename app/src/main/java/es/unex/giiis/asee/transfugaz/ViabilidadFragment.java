package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.vm.ViabilidadFragmentViewModel;

public class ViabilidadFragment extends Fragment {

    private static final String LOG_TAG = ViabilidadFragment.class.getSimpleName();
    public static final String NAV_ARG_ID_OFERTA = "idOferta";

    // Velocidad media de los camiones de reparto asumida para calcular la estimación de tiempo
    private final float VELOCIDAD_MEDIA = 60;
    // Distancia de viaje para la que se considera una viabilidad temporal del 0%
    private final int PEOR_DISTANCIA = 1200;
    // Remuneración para la que se considera una viabilidad económica del 100%
    private final int MEJOR_REMUNERACIÓN = 5000;

    // ID de la oferta cuya viabilidad estamos calculando
    private OfertaViaje mOferta;

    // Elementos de la vista
    TextView textoVTemporal;
    TextView textoVEconómica;
    TextView textoVTotal;
    ProgressBar barraVTotal;
    Button botónAceptar;
    Button botónRechazar;
    TextView mTvCliente;
    TextView mTvDistancia;
    TextView mTvTiempoEstimado;
    TextView mTvRemuneracion;

    ViabilidadFragmentViewModel mViewModel;

    public ViabilidadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_viabilidad_oferta, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Obtener el ID de la oferta para la que estamos calculando la viabilidad
        long idOferta = (long) getArguments().getSerializable(NAV_ARG_ID_OFERTA);

        // Obtener referencias a los elementos de la vista
        textoVTemporal = view.findViewById(R.id.v_temporal_valor);
        textoVEconómica = view.findViewById(R.id.v_económica_valor);
        textoVTotal = view.findViewById(R.id.v_total_valor);
        barraVTotal = view.findViewById(R.id.v_total_barra);
        botónAceptar = view.findViewById(R.id.btn_aceptar);
        botónRechazar = view.findViewById(R.id.btn_rechazar);
        mTvCliente = view.findViewById(R.id.cliente);
        mTvDistancia = view.findViewById(R.id.distancia);
        mTvTiempoEstimado = view.findViewById(R.id.tiempoEstimado);
        mTvRemuneracion = view.findViewById(R.id.remuneración);

        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        mViewModel = new ViewModelProvider(this, contenedorDep.appVMFactory)
                .get(ViabilidadFragmentViewModel.class);

        // Especificar el identificador de la oferta de viaje de la que se quiere calcular su viabilidad
        mViewModel.setIdentificadorOferta(idOferta);

        // Obsevar la oferta de viaje
        mViewModel.getOfertaViaje()
                .observe(getViewLifecycleOwner(), this::alCargarOfertaViaje);
    }

    /**
     * Callback para manejar cuando la oferta de viaje para la que se calcula la viabilidad cambia
     *
     * @param ofertaViaje La oferta de viaje
     */
    private void alCargarOfertaViaje(OfertaViaje ofertaViaje) {
        // Almacenar localmente la oferta de viaje porque es necesaria para crear el viaje asociado
        mOferta = ofertaViaje;

        // Rellenar los campos con los datos de la oferta
        mTvCliente.setText(ofertaViaje.getCliente());
        mTvDistancia.setText(getString(R.string.x_km, ofertaViaje.getDistancia()));
        mTvTiempoEstimado.setText(getString(R.string.x_horas, ofertaViaje.getDistancia() / VELOCIDAD_MEDIA));
        mTvRemuneracion.setText(getString(R.string.x_euros, ofertaViaje.getRemuneracion()));
        textoVTemporal.setText(getString(R.string.x_porciento, 0f));
        textoVEconómica.setText(getString(R.string.x_porciento, 0f));
        textoVTotal.setText(getString(R.string.x_porciento, 0f));
        barraVTotal.setProgress(0);

        // Determinar los valores de viabilidad y animarlos
        float porcentVTemporal = Math.max(-100f / PEOR_DISTANCIA * ofertaViaje.getDistancia() + 100, 0);
        float porcentVEconómica = Math.min(100f / MEJOR_REMUNERACIÓN * ofertaViaje.getRemuneracion(), 100);
        float porcentVTotal = (porcentVTemporal + porcentVEconómica) / 2;
        new Thread(new AnimarViabilidad(porcentVTemporal, porcentVEconómica, porcentVTotal)).start();

        botónAceptar.setOnClickListener(v -> guardarDecisión(true));
        botónRechazar.setOnClickListener(v -> guardarDecisión(false));
    }

    /**
     * Confirma la decisión de aceptar o rechzazar la oferta
     *
     * @param aceptada True si la oferta ha sido aceptada, false si ha sido rechazada
     */
    private void guardarDecisión(boolean aceptada) {
        // Tenemos que obtener la versión BD de la oferta actual y actualizarla
        UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();

        AppExecutors.getInstance().diskIO().execute(() -> {
            // Actualizar el estado de la oferta con la decisión del usuario
            mViewModel.cambiarEstadoOfertaBD(aceptada ? EstadoOferta.ACEPTADA : EstadoOferta.RECHAZADA);

            // Si la oferta es aceptada crear un nuevo viaje y mostrar el detalle del viaje asociado

            if (aceptada) {
                AppExecutors.getInstance().diskIO().execute(() -> {

                    Usuario conductorAsignado = null;

                    // Asignar conductor

                    // Crear lista de potenciales conductores (roles CONDUCTOR y TCA)
                    List<Usuario> conductores = new ArrayList<>();
                    conductores.addAll(daoUsuario.getPorRolNLD(Rol.CONDUCTOR.ordinal()));
                    conductores.addAll(daoUsuario.getPorRolNLD(Rol.TCA.ordinal()));

                    // Recuperar un conductor, si hay alguno disponible
                    if (conductores.size() > 0)
                        conductorAsignado = conductores.get(new Random().nextInt(conductores.size()));

                    // Instanciar Calendar con la fecha de descarga
                    Calendar fechaDescarga = Calendar.getInstance();
                    fechaDescarga.setTimeInMillis(mOferta.getFechaEntrega() * 1000);
                    // Formar ID como 3 primeras letras del origen + 3 primeras letras del destino + día del mes
                    String idViaje = mOferta.getOrigen().substring(0, 3).toUpperCase() +
                            mOferta.getDestino().substring(0, 3).toUpperCase() +
                            fechaDescarga.get(Calendar.DAY_OF_MONTH);

                    // CREAR EL NUEVO VIAJE

                    Viaje nuevoViaje = new Viaje(idViaje,
                            conductores.size() == 0 ? null : conductorAsignado.getId(),
                            mOferta.getOrigen(),
                            mOferta.getDestino(), fechaDescarga, mOferta.getMercancia(),
                            mOferta.getDistancia(), false);

                    // INSERTAR NUEVO VIAJE EN BD LOCAL
                    /*
                     * Ojo: A diferencia de la mayoría de métodos de los VM, este método es síncrono. Esto es necesario ya que
                     * no podemos continuar a la pantalla de detalles del viaje recién creado hasta que no esté insertado en la BD.
                     */
                    mViewModel.crearViaje(nuevoViaje);

                    // Ir al detalle del nuevo viaje
                    MainActivity mainActivity = (MainActivity) getActivity();
                    Bundle args = new Bundle();
                    args.putSerializable(DetallesViajeFragment.ARG_VIAJE_DETALLE, nuevoViaje);
                    mainActivity.runOnUiThread(() ->
                            mainActivity.getNavController()
                                    .navigate(R.id.navegacion_viabilidad_a_detalleviaje, args));
                });
            } else {
                /*
                 * Si la oferta es rechazada, volver a la pantalla de detalle de la oferta simulando la pulsación
                 * del botón back
                 */
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.runOnUiThread(() -> mainActivity.getNavController().popBackStack());
            }
        });
    }

    /**
     * Clase usada para animar los porcentajes en el cálculo de viabilidad de una oferta
     */
    public class AnimarViabilidad implements Runnable {
        float porcentVTemporal;
        float porcentVEconómica;
        float porcentVTotal;

        public AnimarViabilidad(float porcentVTemporal, float porcentVEconómica, float porcentVTotal) {
            this.porcentVTemporal = porcentVTemporal;
            this.porcentVEconómica = porcentVEconómica;
            this.porcentVTotal = porcentVTotal;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                float vTemporalMostrada = Math.min(porcentVTemporal, i);
                float vEconómicaMostrada = Math.min(porcentVEconómica, i);
                float vFinalMostrada = Math.min(porcentVTotal, (vTemporalMostrada + vEconómicaMostrada) / 2);
                getActivity().runOnUiThread(() -> {
                    textoVTemporal.setText(getString(R.string.x_porciento, vTemporalMostrada));
                    textoVEconómica.setText(getString(R.string.x_porciento, vEconómicaMostrada));
                    textoVTotal.setText(getString(R.string.x_porciento, vFinalMostrada));
                    barraVTotal.setProgress(Math.round(vFinalMostrada));
                });
                // Continuar hasta que los 3 valores mostrados alcancen su valor definitivo
                if (vTemporalMostrada == porcentVTemporal && vEconómicaMostrada == porcentVEconómica
                        && vFinalMostrada == porcentVTotal) {
                    break;
                } else {
                    try {
                        Thread.sleep(3000L / 100);
                    } catch (InterruptedException e) {
                        Log.e(LOG_TAG, "La animación del cálculo de viabilidad falló");
                        textoVTemporal.setText(getString(R.string.x_porciento, porcentVTemporal));
                        textoVEconómica.setText(getString(R.string.x_porciento, porcentVEconómica));
                        textoVTotal.setText(getString(R.string.x_porciento, porcentVTotal));
                        barraVTotal.setProgress(Math.round(porcentVTotal));
                        break;
                    }
                }
            }

            // Una vez que termine la animación se reactivan los botones
            getActivity().runOnUiThread(() -> {
                botónAceptar.setEnabled(true);
                botónRechazar.setEnabled(true);
            });
        }
    }
}