package es.unex.giiis.asee.transfugaz;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.unex.giiis.asee.transfugaz.api.OfertasViajeAPIDataSource;
import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.roomdb.OfertaViajeAgregadoDAO;

/**
 * Patrón Repository para manipular el dominio agregado de ofertas de viaje. El dominio agregado
 * incluye la representación de una oferta de viaje proporcionada por la API REST empresarial
 * ({@link OfertaViaje}), y su representación local en BD que almacena el estado de la oferta
 * ({@link OfertaViaje.OfertaViajeDB})
 * <p>
 * Implementa una política de caché sobre la recuperación de ofertas de viaje de la API REST
 * empresarial: reutiliza las ofertas de viaje recuperadas dentro de una ventana de 1 minuto.
 * La fuente de verdad (SSOT) es la caché local implementada en Room.
 */
public class OfertaViajeAgregadoRepository {

    private static final String LOG_TAG = OfertaViajeAgregadoRepository.class.getSimpleName();

    // Tiempo de vida de la caché de ofertas de viaje en milisegundos
    private static final long TIEMPO_VIDA_CACHE_MILIS = 60000;

    private static OfertaViajeAgregadoRepository sInstance;
    private final OfertaViajeAgregadoDAO mOfertasViajeAgregadoDAO;
    private final OfertasViajeAPIDataSource mOfertasViajeAPIDataSource;
    private final AppExecutors mExecutors;
    private Long mTiempoUltimaActualizacion; // Momento en el que se actualizó la caché por última vez

    private OfertaViajeAgregadoRepository(OfertaViajeAgregadoDAO DAO, OfertasViajeAPIDataSource APIDataSource) {
        mOfertasViajeAgregadoDAO = DAO;
        mOfertasViajeAPIDataSource = APIDataSource;
        mExecutors = AppExecutors.getInstance();
        mTiempoUltimaActualizacion = 0L;

        // Observar las ofertas recuperadas de la API para mantener actualizada la caché en Room
        mOfertasViajeAPIDataSource.getListaOfertasActual()
                .observeForever(ofertaViajes -> {
                    mExecutors.diskIO().execute(() -> {
                        // Actualizar la caché local
                        actualizarCacheLocal(ofertaViajes);
                        // Crear la representación del estado de las nuevas ofertas recuperadas
                        actualizarOfertasViajeBDLocales(ofertaViajes);
                    });
                });
    }

    // INTERFAZ PÚBLICA para MANIPULAR el DOMINIO AGREGADO de ofertas de viaje

    /**
     * Invocar este método recupera las ofertas de viaje disponibles en la API REST empresarial
     * aplicando la política de caché
     */
    public void solicitarRecuperarOfertasAPI() {
        mExecutors.diskIO().execute(() -> {
            // Actualizar la caché solo si es necesario
            if (hayQueActualizarCache())
                recuperarOfertasAPI();
        });
    }

    /**
     * @return Un LiveData para observar los cambios sobre colección de ofertas de viaje que son
     * recuperadas de la API REST ({@link OfertaViaje})
     */
    public LiveData<List<OfertaViaje>> getOfertasActuales() {
        // Utilizar Room para devolver LiveData con todas las ofertas cachea
        return mOfertasViajeAgregadoDAO.getTodasOfertas();
    }

    public LiveData<OfertaViaje> getOfertaPorId(long idOferta) {
        return mOfertasViajeAgregadoDAO.getOfertaPorId(idOferta);
    }

    public LiveData<OfertaViaje.OfertaViajeDB> getOfertaBDPorId(long idOferta) {
        return mOfertasViajeAgregadoDAO.getOfertaBDPorId(idOferta);
    }

    /**
     * Cambiar el estado de una oferta de viaje a 'nuevo estado'
     *
     * @param idOferta    Identificador de la oferta de la que se quiere cambiar el estado
     * @param nuevoEstado Nuevo estado de aceptación de la oferta
     */
    public void cambiarEstadoOfertaBD(Long idOferta, EstadoOferta nuevoEstado) {
        mExecutors.diskIO().execute(() -> {
            OfertaViaje.OfertaViajeDB ofertaViajeBD =
                    mOfertasViajeAgregadoDAO.getOfertaBDPorId_NLD(idOferta);
            ofertaViajeBD.setEstado(nuevoEstado);
            mOfertasViajeAgregadoDAO.actualizarOfertaBD(ofertaViajeBD);
        });
    }

    public synchronized static OfertaViajeAgregadoRepository getInstance(
            OfertaViajeAgregadoDAO DAO, OfertasViajeAPIDataSource APIDataSource) {

        if (sInstance == null) {
            sInstance = new OfertaViajeAgregadoRepository(DAO, APIDataSource);
            Log.d(LOG_TAG, "Nuevo repositorio creado");
        }

        return sInstance;
    }

    // INTERFAZ PRIVADA

    /**
     * Reemplaza el contenido la caché de ofertas de viaje con una nueva colección de ofertas
     *
     * @param nuevasOfertas Nueva colección de ofertas de viaje que deben reemplazar el
     *                      contenido de la caché
     */
    private void actualizarCacheLocal(List<OfertaViaje> nuevasOfertas) {
        // Actualizar la caché solo si la colección de nuevas ofertas no está vacía
        // CASO EXTREMO: Si se borran todas las ofertas en la API REST no se actualiza la caché
        if (nuevasOfertas != null && nuevasOfertas.size() > 0) {
            // Estrategia de actualización: vaciar y reemplazar
            mOfertasViajeAgregadoDAO.borrarTodasOfertas();
            mOfertasViajeAgregadoDAO.insertarOfertasPorLotes(nuevasOfertas);

            Log.d(LOG_TAG, String.format(
                    "Caché de OfertaViaje en Room actualizada. Ahora contiene (%d) entradas",
                    nuevasOfertas.size()));
        }
    }

    /**
     * Crea un objeto {@link OfertaViaje} para almacenar el estado de aceptación de nuevas
     * ofertas de viaje, aquellas que están en la lista 'nuevasOfertas' pero no en la BD de Room local
     *
     * @param nuevasOfertas Lista con ofertas de viaje potencialmente nuevas
     */
    private void actualizarOfertasViajeBDLocales(List<OfertaViaje> nuevasOfertas) {
        List<OfertaViaje.OfertaViajeDB> ofertasLocales =
                mOfertasViajeAgregadoDAO.getTodasOfertasBD_NLD();
        Set<Long> IDsOfertasLocales = new HashSet<>();

        // Determinar qué nuevas ofertas se deben crear en la base de datos local

        // IDs de las ofertas ya almacenadas
        if (ofertasLocales != null && ofertasLocales.size() > 0) {
            IDsOfertasLocales = ofertasLocales.stream()
                    .map(OfertaViaje.OfertaViajeDB::getId)
                    .collect(Collectors.toSet());
        }

        // IDs de nuevas ofertas que provienen de la API REST
        Set<Long> IDsOfertasRemotas = nuevasOfertas.stream()
                .map(OfertaViaje::getId)
                .collect(Collectors.toSet());

        // Averiguar qué ofertas son nuevas
        IDsOfertasRemotas.removeAll(IDsOfertasLocales);

        // Registrar nuevas ofertas en la BD
        for (Long id : IDsOfertasRemotas) {
            mOfertasViajeAgregadoDAO.insertarOfertaBD(
                    new OfertaViaje.OfertaViajeDB(id, EstadoOferta.PENDIENTE));
        }

        Log.d(LOG_TAG, String.format(
                "Insertados (%d) nuevos objetos OfertaViajeBD", IDsOfertasRemotas.size()));
    }

    /**
     * Política de caché de ofertas de viaje ({@link OfertaViaje}): cuando han pasado más de
     * TIEMPO_VIDA_CACHE_MILIS milisegundos desde la última actualización o la caché está vacía
     *
     * @return Si es necesario actualizar la caché de ofertas de viaje
     */
    private boolean hayQueActualizarCache() {
        long tiempoDesdeUltimaActualizacion = System.currentTimeMillis() - mTiempoUltimaActualizacion;

        // Actualizar cuando han pasado más de TIEMPO_VIDA_CACHE_MILIS milisegundos o la caché está vacía
        return tiempoDesdeUltimaActualizacion > TIEMPO_VIDA_CACHE_MILIS ||
                mOfertasViajeAgregadoDAO.cuantasOfertas() == 0;
    }

    /**
     * Fuerza la recuperación de ofertas de viaje de la fuente de datos de la API REST empresarial,
     * ignorando la política de caché
     */
    private void recuperarOfertasAPI() {
        Log.d(LOG_TAG, "Recuperando lista de ofertas de viaje de la API REST ...");

        mExecutors.networkIO().execute(() -> {
            // Consultar la API REST de ofertas de viaje empresarial
            mOfertasViajeAPIDataSource.recuperarOfertasAPI();
            // Actualizar el momento de la última actualización de la caché
            mTiempoUltimaActualizacion = System.currentTimeMillis();
        });
    }
}
