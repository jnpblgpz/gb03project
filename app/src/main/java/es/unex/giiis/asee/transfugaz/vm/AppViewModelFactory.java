package es.unex.giiis.asee.transfugaz.vm;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.transfugaz.OfertaViajeAgregadoRepository;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.api.OfertasViajeAPIDataSource;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;

/**
 * Implementación de la factoría de creación de ViewModels. Crea los diferentes ViewModel necesarios por los fragmentos de la
 * aplicación y les pasa las dependencias que necesiten (los repositorios).
 * Los repositorios se crean como atributos privados de esta clase y por tanto no son accesibles desde fuera, evitando así que
 * alguna otra clase que no sea un ViewModel creado aquí pueda acceder directamente a un repositorio, lo que rompería la
 * arquitectura por capas.
 */
public class AppViewModelFactory extends ViewModelProvider.NewInstanceFactory {

	private OfertaViajeAgregadoRepository repoOfertas;
	private UsuarioRepository repoUsuarios;
	private ViajeRepository repoViajes;

	/**
	 * Crea la instancia de la factoría. Recibe como parámetros los objetos necesarios para crear todos los repositorios de
	 * la aplicación
	 * @param db La instancia única de la base de datos de la aplicación
	 * @param fuenteDatosAPI La fuente de datos de la API de ofertas
	 */
	public AppViewModelFactory(TransFugazDB db, OfertasViajeAPIDataSource fuenteDatosAPI) {
		repoOfertas = OfertaViajeAgregadoRepository.getInstance(db.getDaoOfertaViajeAgregado(), fuenteDatosAPI);
		repoUsuarios = UsuarioRepository.getInstance(db.getDaoUsuario());
		repoViajes = ViajeRepository.getInstance(db.getDaoViaje());
	}

	@SuppressWarnings("unchecked")
	@NonNull
	@Override
	public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
		if (modelClass.equals(OfertasFragmentViewModel.class)) {
			return (T) new OfertasFragmentViewModel(repoOfertas);
		} else if (modelClass.equals(DetallesOfertaFragmentViewModel.class)) {
			return (T) new DetallesOfertaFragmentViewModel(repoOfertas);
		} else if (modelClass.equals(ViabilidadFragmentViewModel.class)) {
			return (T) new ViabilidadFragmentViewModel(repoOfertas, repoViajes);
		} else if (modelClass.equals(CrearUsuarioVM.class)) {
			return (T) new CrearUsuarioVM(repoUsuarios);
		} else if (modelClass.equals(EditarUsuarioVM.class)) {
			return (T) new EditarUsuarioVM(repoUsuarios);
		} else if (modelClass.equals(ListaUsuariosVM.class)) {
			return (T) new ListaUsuariosVM(repoUsuarios);
		} else if (modelClass.equals(ListaViajesVM.class)) {
			return (T) new ListaViajesVM(repoViajes);
		} else if (modelClass.equals(DetallesViajeVM.class)) {
			return (T) new DetallesViajeVM(repoViajes);
		} else if (modelClass.equals(EditarViajeVM.class)) {
			return (T) new EditarViajeVM(repoViajes);
		} else if (modelClass.equals(InformeViajeVM.class)) {
			return (T) new InformeViajeVM(repoViajes);
		} else {
			return super.create(modelClass);
		}
	}
}
