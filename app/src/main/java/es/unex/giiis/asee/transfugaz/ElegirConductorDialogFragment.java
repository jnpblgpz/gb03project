package es.unex.giiis.asee.transfugaz;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Collections;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;

/**
 * AlertDialog que permite al usuario seleccionar un conductor entre todos los conductores de la
 * base de datos local
 */
public class ElegirConductorDialogFragment extends DialogFragment {

    // Callback invocado cuando el usuario elija un conductor de la lista
    private OnConductorSelectedListener mOnConductorSelectedListener;
    private AdaptadorListaConductores mAdaptadorListaConductores;

    public ElegirConductorDialogFragment(OnConductorSelectedListener mOnConductorSelectedListener) {
        this.mOnConductorSelectedListener = mOnConductorSelectedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();
        mAdaptadorListaConductores =
                new AdaptadorListaConductores(getContext(), android.R.layout.simple_list_item_1);

        AppExecutors.getInstance().diskIO().execute(() -> {
            // Todos los Rol.CONDUCTOR
            List<String> nombresPorRol = daoUsuario.getNombresPorRol(Rol.CONDUCTOR.ordinal());
            // Todos los Rol.TCA
            nombresPorRol.addAll(daoUsuario.getNombresPorRol(Rol.TCA.ordinal()));
            // Añadir la opción para no aplicar el filtro
            nombresPorRol.add(getResources().getString(R.string.viajes_filtro_ninguno));

            mAdaptadorListaConductores.reemplazar(nombresPorRol);
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.accion_filtrar_viajes)
                .setAdapter(mAdaptadorListaConductores, (dialog, which) ->
                        mOnConductorSelectedListener.onConductorSelected(
                                mAdaptadorListaConductores.consultar().get(which)));
        return builder.create();
    }

    public interface OnConductorSelectedListener {
        void onConductorSelected(String nombreConductor);
    }

    /**
     * ListAdapter para popular la lista de conductores posibles en ElegirConductorDialogFragment
     */
    private static class AdaptadorListaConductores extends ArrayAdapter<String> {

        private List<String> mNombresConductor;

        public AdaptadorListaConductores(@NonNull Context context, int resource) {
            super(context, resource);
        }

        public void reemplazar(List<String> nombresConductores) {
            if (mNombresConductor != null) mNombresConductor.clear();
            mNombresConductor = nombresConductores;
            notifyDataSetChanged();
        }

        @Nullable
        @Override
        public String getItem(int position) {
            return mNombresConductor.get(position);
        }

        public List<String> consultar() {
            return Collections.unmodifiableList(mNombresConductor);
        }

        @Override
        public int getCount() {
            return mNombresConductor.size();
        }
    }
}
