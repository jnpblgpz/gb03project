package es.unex.giiis.asee.transfugaz;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;
import es.unex.giiis.asee.transfugaz.vm.DetallesViajeVM;

/**
 * Diálogo para confirmar el borrado de un viaje
 */
public class BorrarViajeDialogFragment extends DialogFragment {

	private final Viaje mViaje;

	/*
	 * ViewModel asociado a la pantalla de detalles de viaje.
	 * Esta clase no puede tener su propio ViewModel ya que es un fragmento "detached".
	 */
	private DetallesViajeVM detallesViajeVM;

	public BorrarViajeDialogFragment(Viaje viaje, DetallesViajeVM detallesViajeVM) {
		// Required empty public constructor
		mViaje = viaje;
		this.detallesViajeVM = detallesViajeVM;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity())
			.setMessage(R.string.msg_borrarViaje) // Mensaje de advertencia
			// Confirmar el borrado
			.setPositiveButton(R.string.viaje_btn_borrar_ok, (dialog, which) -> {
				ViajeDAO daoViaje = TransFugazDB.getInstance(getContext()).getDaoViaje();
				MainActivity mainActivity = (MainActivity) getActivity();

				// Borrar el viaje de la BD local
				detallesViajeVM.borrarViaje(mViaje.getId());

				// Navegar de vuelta a la pestaña de Transporte
				mainActivity.runOnUiThread(() -> mainActivity.getNavController()
					.navigate(R.id.navegacion_detalleviaje_a_transportes));
			})
			// Cancelar el borrado
			.setNegativeButton(R.string.viaje_btn_borrar_cancelar, (dialog, which) -> dismiss())
			.create();
	}
}