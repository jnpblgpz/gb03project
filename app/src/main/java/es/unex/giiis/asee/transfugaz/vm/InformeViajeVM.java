package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.model.Viaje;

public class InformeViajeVM extends ViewModel {
	private ViajeRepository repositorio;

	public InformeViajeVM(ViajeRepository repositorio) {
		this.repositorio = repositorio;
	}

	public void setFinalizado(Viaje viaje, boolean finalizado) {
		repositorio.setFinalizado(viaje, finalizado);
	}
}
