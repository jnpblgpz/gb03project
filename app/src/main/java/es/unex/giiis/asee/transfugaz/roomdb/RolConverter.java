package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.room.TypeConverter;

import es.unex.giiis.asee.transfugaz.model.Rol;

public class RolConverter {
	@TypeConverter
	public static Integer toInteger(Rol r) {
		return r == null ? null : r.ordinal();
	}

	@TypeConverter
	public static Rol toRol(Integer i) {
		return i == null ? null : Rol.values()[i];
	}
}
