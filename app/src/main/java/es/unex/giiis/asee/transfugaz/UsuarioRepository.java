package es.unex.giiis.asee.transfugaz;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.unex.giiis.asee.transfugaz.api.OfertasViajeAPIDataSource;
import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.roomdb.RolConverter;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;

/**
 * Patrón Repository para manipular datos de usuarios
 */
public class UsuarioRepository {
	private static UsuarioRepository instancia;
	private final UsuarioDAO usuarioDAO;

	private UsuarioRepository(UsuarioDAO DAO) {
		usuarioDAO = DAO;
	}

	public synchronized static UsuarioRepository getInstance(UsuarioDAO DAO) {
		if (instancia == null) {
			instancia = new UsuarioRepository(DAO);
		}
		return instancia;
	}

	/**
	 * @return Un LiveData para observar los cambios sobre una colección de usuarios leídos de la BD
	 */
	public LiveData<List<Usuario>> getUsuarios() {
		return usuarioDAO.getTodos();
	}

	public LiveData<Usuario> getPorId(Long id) {
		return usuarioDAO.getPorId(id);
	}

	public LiveData<Usuario> getPorNombre(String nombre) {
		return usuarioDAO.getPorNombre(nombre);
	}

	public LiveData<List<Usuario>> getPorRol(Rol rol) {
		return usuarioDAO.getPorRol(RolConverter.toInteger(rol));
	}

	public long insertar (Usuario usuario) {
		return usuarioDAO.insertar(usuario);
	}

	public int actualizar(Usuario usuario) {
		return usuarioDAO.actualizar(usuario);
	}

	public void borrar(Long id) {
		usuarioDAO.borrar(id);
	}

	public void borrar(String nombre) {
		usuarioDAO.borrar(nombre);
	}
}
