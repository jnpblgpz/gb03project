package es.unex.giiis.asee.transfugaz.api;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

/**
 * Listener notificado cuando la API REST de ofertas devuelve una lista de ofertas de viaje
 */
public interface OnListaOfertasLoadedListener {

    void onListaOfertasLoaded(List<OfertaViaje> listaOfertas);

}
