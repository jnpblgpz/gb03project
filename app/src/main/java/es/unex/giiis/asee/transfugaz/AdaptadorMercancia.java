package es.unex.giiis.asee.transfugaz;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Adaptador para alimentar la RecyclerView que muestra la lista de ítems en la mercancía de un viaje.
 * Para utilizarlo junto a una lista de ítems de solo lectura utilizar 'esEditable' = false,
 * y para utilizarlo junto a una lista editable de ítems utilizar 'esSimple' es true.
 * <p>
 * Para cargar datos en el adaptador utilizar {@link #cargarItems(List)}. Para consultar la lista
 * de ítems actual utilizar {@link #consultarMercancia()}
 */
public class AdaptadorMercancia extends RecyclerView.Adapter<AdaptadorMercancia.ViewHolder> {

    private List<String> listaItems; // Ítems en la partida de mercancia
    private final boolean esEditable; // Indica si se deben proporcionar ítems que se puedan borrar

    public AdaptadorMercancia(boolean esEditable) {
        this.esEditable = esEditable;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(
                parent.getContext())
                // Elegir el layout adecuado en función de si la lista es editable o no
                .inflate(esEditable ? R.layout.item_mercancia : R.layout.item_mercancia_simple,
                        parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Inicializar un ViewHolder con el ítem en la posición 'position'
        holder.bind(listaItems.get(position));
    }

    @Override
    public int getItemCount() {
        return listaItems.size();
    }

    /**
     * Reemplaza la colección de ítems
     *
     * @param items Nueva colección de ítems
     */
    public void cargarItems(List<String> items) {
        if (listaItems != null) listaItems.clear(); // Vaciar la lista actual para liberar las referencias
        /*
         * REEMPLAZA la colección actual
         * La lista se copia para evitar un crasheo al intentar modificarla si es de solo lectura (puede ocurrir si la lista
         * recibida por parámetro se ha obtenido de consultarMercancía)
         */
        listaItems = new ArrayList<String>(items);

        notifyDataSetChanged();
    }

    /**
     * @return Lista de solo lectura (unmodifiable) con los ítems almacenados actualmente
     */
    public List<String> consultarMercancia() {
        return Collections.unmodifiableList(listaItems);
    }

    public void agregarItem(String item) {
        listaItems.add(item);

        notifyDataSetChanged();
    }

    /**
     * @param adapterPosition Posición asoluta que ocupa un ítem en este adaptador
     */
    public void eliminarItem(int adapterPosition) {
        listaItems.remove(adapterPosition);

        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Vistas
        private ImageView mImgBorrarItem;
        private TextView mTvItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            // Conseguir referencias a vistas

            // Solo si la lista es editable
            if (esEditable)
                mImgBorrarItem = itemView.findViewById(R.id.img_viaje_mercancia_borrarItem);

            mTvItem = itemView.findViewById(R.id.tv_viaje_mercancia_item);
        }

        public void bind(final String item) {
            mTvItem.setText(item); // Nombre del ítem

            // Al hacer clic en esta imagen se debe borrar el ítem del adaptador (SOLO SI ES EDITABLE)
            if (esEditable)
                mImgBorrarItem.setOnClickListener(v -> eliminarItem(getAdapterPosition()));
        }
    }

}
