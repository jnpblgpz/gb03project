package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

public class PaginacionOfertasFragment extends Fragment {

    public static final String NAV_ARG_ID_OFERTA = "idOferta";
    public static final String NAV_ARG_LISTA_OFERTAS = "listaOfertas";

    private ViewPager2 mVp2PaginacionOfertas;
    private ColeccionOfertasAdapter mOfertasAdapter;
    private List<OfertaViaje> mListaOfertas;

    public PaginacionOfertasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_paginacion_ofertas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Recuperar el Identificador de la oferta de viaje que debería mostrarse
        long idOferta = (long) getArguments().getSerializable(NAV_ARG_ID_OFERTA);
        //noinspection unchecked
        mListaOfertas = (List<OfertaViaje>) getArguments().getSerializable(NAV_ARG_LISTA_OFERTAS);

        mVp2PaginacionOfertas = view.findViewById(R.id.vp2_paginacionOfertas);
        mOfertasAdapter = new ColeccionOfertasAdapter(this);
        mVp2PaginacionOfertas.setAdapter(mOfertasAdapter);
        // Averiguar la posición que ocupa la oferta y mostrar el detalle correcto
        mVp2PaginacionOfertas.setCurrentItem(idOfertaAIndice(idOferta), false);
    }

    /**
     * Traduce el identificador de una oferta al índice que ocupa dentro de la lista de ofertas de
     * viaje Esta operación permite conocer en qué posición está situada una oferta de viaje
     * actualmente, pero en el futuro puede cambiar
     *
     * @param idOferta Identificador de la oferta que se quiere situar como un índice
     * @return El índice que ocupa la oferta de viaje asociada al identificador, o -1 si no se pudo
     * encontrar ninguna oferta con el identificador proporcionado
     */
    private int idOfertaAIndice(long idOferta) {
        // Buscar la oferta que coincide con el ID y devolver el índice 'idx'
        for (int idx = 0; idx < mListaOfertas.size(); idx++) {
            if (mListaOfertas.get(idx).getId() == idOferta) return idx;
        }

        // No se pudo encontrar la oferta por el identificador proporcionado
        return -1;
    }

    /**
     * Traduce el índice que ocupa una oferta de viaje de la lista de ofertas de viaje en su
     * identificador asociado. Esta operación permite conocer en qué oferta de viaje ocupa una
     * posición actualmente, pero en el futuro puede cambiar.
     *
     * @param indice Índice en la tabla 'OfertaViaje' de la BD local Room
     * @return El identificador de la oferta de viaje que corresponde con la posición proporcionada,
     * o -1 si la posición no es válida
     */
    private long indiceAIdOferta(int indice) {
        // Comprobar que el índice sea válido
        if (indice >= 0 && indice < mListaOfertas.size()) {
            return mListaOfertas.get(indice).getId();
        } else {
            return -1; // Índice invalido
        }
    }

    class ColeccionOfertasAdapter extends FragmentStateAdapter {

        public ColeccionOfertasAdapter(@NonNull Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            DetallesOfertaFragment detallesOfertaFragment = new DetallesOfertaFragment();
            Bundle args = new Bundle();
            args.putSerializable(DetallesOfertaFragment.ARG_ID_VIAJE_DETALLE,
                    indiceAIdOferta(position));
            detallesOfertaFragment.setArguments(args);
            return detallesOfertaFragment;
        }

        @Override
        public int getItemCount() {
            return mListaOfertas.size();
        }
    }
}