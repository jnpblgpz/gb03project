package es.unex.giiis.asee.transfugaz.api;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import retrofit2.Retrofit;

/**
 * Runnable para recuperar todas las ofertas de viaje almacenadas en la API REST de ofertas
 */
public class ListaOfertasLoader implements Runnable {

    private static final String LOG_TAG = ListaOfertasLoader.class.getSimpleName();

    private final Retrofit mRetrofit;
    private final OnListaOfertasLoadedListener mOnListaOfertasLoadedListener;

    public ListaOfertasLoader(Retrofit retrofit, OnListaOfertasLoadedListener mOnListaOfertasLoadedListener) {
        this.mRetrofit = retrofit;
        this.mOnListaOfertasLoadedListener = mOnListaOfertasLoadedListener;
    }

    @Override
    public void run() {

        // Inicializar el servicio de ofertas con Retrofit

        OfertasViajeService servicioOfertas = mRetrofit.create(OfertasViajeService.class);

        // Recuperar todas las ofertas de viaje de la API REST

        try {
            List<OfertaViaje> ofertas = servicioOfertas.listarOfertas().execute().body();
            AppExecutors
                    .getInstance()
                    .mainThread()
                    .execute(() -> mOnListaOfertasLoadedListener.onListaOfertasLoaded(ofertas));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error al recuperar las ofertas de viaje más recientes");
        }
    }
}
