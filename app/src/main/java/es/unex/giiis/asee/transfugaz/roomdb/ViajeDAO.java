package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Viaje;

@Dao
public interface ViajeDAO {

    /**
     * @return Todos los viajes almacenados
     */
    @Query("SELECT * FROM Viaje")
    LiveData<List<Viaje>> getTodos();

    /**
     * @param idConductor ID del conductor
     * @return Todos los viajes asignados a un conductor en particular
     */
    @Query("SELECT * FROM Viaje WHERE idConductor = :idConductor")
    LiveData<List<Viaje>> getPorConductor(final Long idConductor);

    /**
     * @param id ID alfanumérico del viaje
     * @return El viaje correspondiente al ID proporcionado
     */
    @Query("SELECT * FROM Viaje WHERE id = :id")
    LiveData<Viaje> getPorId(String id);

    // Métodos que devuelven datos directos simples sin usar LiveData. Usados para rellenar inputs u otras tareas de acceso único

    @Query("SELECT * FROM Viaje WHERE id = :id")
    Viaje getPorIdNLD(String id);

    // Modificaciones

    @Insert
    long insertar(Viaje viaje);

    @Update
    int actualizar(Viaje viaje);

    /**
     * Borrar un viaje de la BD por su ID
     *
     * @param id ID alfanumérico del viaje
     */
    @Query("DELETE FROM Viaje WHERE id = :id")
    void borrar(String id);

}
