package es.unex.giiis.asee.transfugaz.vm;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;

/**
 * ViewModel para el fragmento ViajesFragment
 */
public class ListaViajesVM extends ViewModel {

	private ViajeRepository repositorio;
	private LiveData<List<Viaje>> viajes;
	// ID del conductor por el que se están filtrando los viajes. Afecta a qué viajes se muestran en la lista.
	private MutableLiveData<Long> idConductor;

	public ListaViajesVM(ViajeRepository repositorio) {
		this.repositorio = repositorio;
		viajes = repositorio.getViajes();
		idConductor = new MutableLiveData<>();
	}

	public void setConductorFiltrado(Long idConductor) {
		this.idConductor.setValue(idConductor);
	}

	public LiveData<List<Viaje>> getViajes() {
		return Transformations.switchMap(idConductor, input -> {
			if (input == null) {
				return repositorio.getViajes();
			} else {
				return repositorio.getPorConductor(input);
			}
		});
	}
}
