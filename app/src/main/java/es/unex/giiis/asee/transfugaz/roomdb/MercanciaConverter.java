package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Conversor de tipo Room para traducir una lista de mercancías en una cadena separada
 * por punto y coma (;) susceptible de ser almacenada en la base de datos y viceversa
 */
public class MercanciaConverter {

    // Secuencia que separa 2 ítems distintos
    public static final String SEPARADOR_ITEM_MERCANCIA = ";";

    @TypeConverter
    public static List<String> aListaMercancia(String cadenaMercancia) {

        // Si la cadena está vacía devolver una lista vacía
        if (cadenaMercancia == null || cadenaMercancia.trim().isEmpty()) return new ArrayList<>();

        return Arrays.asList(cadenaMercancia
                .trim()
                .split(SEPARADOR_ITEM_MERCANCIA));
    }

    @TypeConverter
    public static String aCadenaMercancia(List<String> listaMercancia) {

        // Si la lista está vacía devolver una cadena vacía
        if (listaMercancia == null || listaMercancia.isEmpty()) return "";

        return listaMercancia.stream()
                .reduce((item1, item2) -> item1.trim() + ";" + item2.trim())
                // Si la lista está vacía devolver una cadena vacía
                .orElse("");
    }

}
