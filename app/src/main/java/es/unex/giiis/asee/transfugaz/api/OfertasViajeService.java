package es.unex.giiis.asee.transfugaz.api;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Cliente REST para la API de ofertas de viaje de Transportes Fugaz SL.
 * http://transportesfugaz-api.eu-west-3.elasticbeanstalk.com/transportesfugaz/rest/ofertas
 */
public interface OfertasViajeService {

    /**
     * @return Una colección con todas las ofertas de viaje registradas en la API REST
     */
    @GET("ofertas")
    Call<List<OfertaViaje>> listarOfertas();

}
