package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Usuario;

@Dao
public interface UsuarioDAO {
	@Query("SELECT * FROM Usuario")
	LiveData<List<Usuario>> getTodos();

	@Query("SELECT * FROM Usuario WHERE id = :id")
	LiveData<Usuario> getPorId(Long id);

	@Query("SELECT * FROM Usuario WHERE nombre = :nombre")
	LiveData<Usuario> getPorNombre(String nombre);

	@Query("SELECT * FROM Usuario WHERE rol = :rol")
	LiveData<List<Usuario>> getPorRol(Integer rol);

	// Métodos que devuelven datos directos simples sin usar LiveData. Usados para rellenar inputs u otras tareas de acceso único

	@Query("SELECT * FROM Usuario WHERE rol = :rol")
	List<Usuario> getPorRolNLD(Integer rol);

	@Query("SELECT * FROM Usuario WHERE id = :id")
	Usuario getPorIdNLD(Long id);

	@Query("SELECT * FROM Usuario WHERE nombre = :nombre")
	Usuario getPorNombreNLD(String nombre);

	@Query("SELECT nombre FROM Usuario WHERE rol = :rol")
	List<String> getNombresPorRol(Integer rol);

	// Modificaciones

	@Insert
	long insertar(Usuario usuario);

	@Update
	int actualizar(Usuario usuario);

	@Query("DELETE FROM Usuario WHERE id = :id")
	void borrar(Long id);

	@Query("DELETE FROM Usuario WHERE nombre = :nombre")
	void borrar(String nombre);
}
