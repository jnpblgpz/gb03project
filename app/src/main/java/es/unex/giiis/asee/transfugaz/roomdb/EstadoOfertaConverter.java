package es.unex.giiis.asee.transfugaz.roomdb;

import androidx.room.TypeConverter;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;

/**
 * Conversor de tipo Room para traducir el estado de una oferta a un valor entero y viceversa
 */
public class EstadoOfertaConverter {

    @TypeConverter
    public static EstadoOferta aEstadoOferta(Integer indiceAsociado) {
        return indiceAsociado == null ? null : EstadoOferta.values()[indiceAsociado];
    }

    @TypeConverter
    public static Integer aIndiceAsociado(EstadoOferta estadoOferta) {
        return estadoOferta == null ? null : estadoOferta.ordinal();
    }

}
