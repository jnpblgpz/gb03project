package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.vm.ListaViajesVM;

public class ViajesFragment extends Fragment
	implements ElegirConductorDialogFragment.OnConductorSelectedListener {

	private RecyclerView recyclerView;
	private RecyclerView.LayoutManager layoutManager;
	private AdaptadorViaje adaptadorViaje;

	private ListaViajesVM listaViajesVM;

	public ViajesFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		// Obtener el VM correspondiente a este fragmento
		AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
		listaViajesVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(ListaViajesVM.class);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_viajes, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Preparar la RecyclerView y el adaptador
		recyclerView = view.findViewById(R.id.rv_viajes);
		recyclerView.setHasFixedSize(true);
		layoutManager = new LinearLayoutManager(getActivity());
		recyclerView.setLayoutManager(layoutManager);
		adaptadorViaje = new AdaptadorViaje((MainActivity) getActivity());
		recyclerView.setAdapter(adaptadorViaje);

		// Fijar el conductor por el que filtrar y observar el LiveData devuelto por el VM
		listaViajesVM.setConductorFiltrado(null);
		listaViajesVM.getViajes().observe(getViewLifecycleOwner(), this::alCargarViajes);
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		inflater.inflate(R.menu.menu_toolbar_viajes, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		// Si se ha seleccionado al acción de filtrar viajes por conductor, construir y mostrar el diálogo
		if (item.getItemId() == R.id.acc_filtrar_viajes) {
			ElegirConductorDialogFragment elegirConductorDialog =
				new ElegirConductorDialogFragment(this);
			elegirConductorDialog.show(getParentFragmentManager(), "elegirConductorDialog");
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	/**
	 * Llamado por el LiveData de viajes cuando se actualizan los viajes de la lista (por ejemplo, sucede al cambiar el filtro de
	 * conductor)
	 * Encargado de pasar la nueva lista de viajes al adaptador
	 * @param viajes Nueva lista de viajes
	 */
	private void alCargarViajes(List<Viaje> viajes) {
		adaptadorViaje.cargar(viajes);
	}

	/**
	 * Callback que actualiza el ViewModel para aplicar el filtro por conductor
	 *
	 * @param nombreConductor El nombre del conductor por el que se deben filtrar los viajes. Si es
	 *                        'ninguno' (R.string.viajes_filtro_ninguno) entonces no aplica ningún
	 *                        filtro
	 */
	@Override
	public void onConductorSelected(String nombreConductor) {
		UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();

		// Comprobar si no se está desactivando el filtro
		if (nombreConductor.equals(getResources().getString(R.string.viajes_filtro_ninguno))) {
			// Volver a mostrar todos los viajes
			listaViajesVM.setConductorFiltrado(null);
		} else {
			// Solo mostrar los viajes del conductor especificado
			AppExecutors.getInstance().diskIO().execute(() -> {
				Long idConductor = daoUsuario.getPorNombreNLD(nombreConductor).getId();
				// Debe ejecutarse en el hilo principal ya que no se permite llamar a MutableLiveData.setValue en otro hilo
				((MainActivity) getActivity()).runOnUiThread(() -> listaViajesVM.setConductorFiltrado(idConductor));
			});
		}
	}
}