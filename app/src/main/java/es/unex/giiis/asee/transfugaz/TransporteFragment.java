package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import es.unex.giiis.asee.transfugaz.model.Usuario;

public class TransporteFragment extends Fragment {
	// Gestión de pestañas
	AdaptadorTabsTransporte adaptadorTransporte;
	ViewPager2 viewPager;

	public TransporteFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_transporte, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
		// Asegurarse de que el usuario está conectado
		Usuario usuarioConectado = ((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado;
		if (usuarioConectado == null) {
			// Redirigir a la pantalla de login
			getActivity().runOnUiThread(() -> {
				NavController navController = Navigation.findNavController(view);
				navController.navigate(R.id.navegacion_a_login);
			});
		} else {
			adaptadorTransporte = new AdaptadorTabsTransporte(this);
			viewPager = view.findViewById(R.id.pager);
			viewPager.setAdapter(adaptadorTransporte);

			TabLayout tabLayout = view.findViewById(R.id.contenedor_pestañas);
			new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
				@Override
				public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
					switch (position) {
						case 0:
							tab.setText(R.string.pantalla_ofertas);
							break;
						case 1:
							tab.setText((R.string.pantalla_viajes));
							break;
						default:
							break;
					}
				}
			}).attach();
		}
	}
}