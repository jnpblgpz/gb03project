package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.OfertaViajeAgregadoRepository;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Usuario;

/**
 * ViewModel para el fragmento UsuariosFragment
 */
public class ListaUsuariosVM extends ViewModel {

    private UsuarioRepository repositorio;
    private LiveData<List<Usuario>> usuarios;

    public ListaUsuariosVM(UsuarioRepository repositorio) {
        this.repositorio = repositorio;
        usuarios = repositorio.getUsuarios();
    }

    public LiveData<List<Usuario>> getUsuarios() {
        return usuarios;
    }

    public void borrarUsuario(Long id) {
        AppExecutors.getInstance()
            .diskIO()
            .execute(() -> {
                repositorio.borrar(id);
            });
    }
}
