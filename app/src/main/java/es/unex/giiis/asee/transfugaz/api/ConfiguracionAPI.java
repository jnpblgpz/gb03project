package es.unex.giiis.asee.transfugaz.api;

public final class ConfiguracionAPI {

    // URL base de la API REST de ofertas
    public static final String TF_OFERTAS_RESTAPI_URL =
            "http://transportesfugaz-api.eu-west-3.elasticbeanstalk.com/transportesfugaz/rest/";

    private ConfiguracionAPI() {
    }

}
