package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.TransFugazDB;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.vm.DetallesViajeVM;

public class DetallesViajeFragment extends Fragment {

    public static final String ARG_VIAJE_DETALLE = "viajeDetalle";

    private Viaje mViajeDetalle; // Viaje del que se muestran los detalles
    private RecyclerView.LayoutManager mRvLayoutManager;
    private AdaptadorMercancia mAdaptadorMercancia;
    // Vistas
    private TextView mTvOrigen;
    private TextView mTvDestino;
    private TextView mTvFechaDescarga;
    private TextView mTvDistancia;
    private TextView mTvConductor;
    private RecyclerView mRvMercancia;
    private TextView mTvEstado;
    private BorrarViajeDialogFragment mBorrarDialog;

    private DetallesViajeVM detallesViajeVM;

    public DetallesViajeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // Habilitar la edición de la Toolbar

        // Obtener el VM correspondiente a este fragmento
        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        detallesViajeVM = new ViewModelProvider(this, contenedorDep.appVMFactory).get(DetallesViajeVM.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalles_viaje, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UsuarioDAO daoUsuario = TransFugazDB.getInstance(getContext()).getDaoUsuario();

        // Recuperar el viaje del que se muestran sus detalles
        mViajeDetalle = (Viaje) getArguments().getSerializable(ARG_VIAJE_DETALLE);

        // Cambiar título de la ActionBar al ID del viaje
        ((MainActivity) getActivity()).actionBarCambiarTitulo(mViajeDetalle.getId());

        // INICIALIZAR REFERENCIAS A VISTAS

        mTvOrigen = view.findViewById(R.id.tv_viaje_detalle_origen);
        mTvDestino = view.findViewById(R.id.tv_viaje_detalle_destino);
        mTvFechaDescarga = view.findViewById(R.id.tv_viaje_detalle_fechadescarga);
        mTvDistancia = view.findViewById(R.id.tv_viaje_detalle_distancia);
        mTvConductor = view.findViewById(R.id.tv_viaje_detalle_conductor);
        mRvMercancia = view.findViewById(R.id.rv_viaje_detalle_mercancia);
        mTvEstado = view.findViewById(R.id.tv_viaje_detalle_estado);

        // CONFIGURAR VISTAS

        // Preparar la RecyclerView y el adaptador para la lista estática de ítems en la mercancía
        mRvLayoutManager = new LinearLayoutManager(getContext());
        mRvMercancia.setLayoutManager(mRvLayoutManager);
        mAdaptadorMercancia = new AdaptadorMercancia(false);
        mAdaptadorMercancia.cargarItems(mViajeDetalle.getMercancia());
        mRvMercancia.setAdapter(mAdaptadorMercancia);

        mBorrarDialog = new BorrarViajeDialogFragment(mViajeDetalle, detallesViajeVM);

        // ASIGNAR VALORES INICIALES A LAS VISTAS

        mTvOrigen.setText(mViajeDetalle.getOrigen());
        mTvDestino.setText(mViajeDetalle.getDestino());
        mTvFechaDescarga.setText(mViajeDetalle.fechaDescargaFormatoExtendido());
        mTvDistancia.setText(Float.toString(mViajeDetalle.getDistancia()));
        AppExecutors.getInstance().diskIO().execute(() -> {
            Usuario conductor = daoUsuario.getPorIdNLD(mViajeDetalle.getIdConductor());
            getActivity().runOnUiThread(() -> mTvConductor.setText(conductor == null ?
                    getResources().getString(R.string.viaje_no_asignado) : conductor.getNombre()));
        });
        mTvEstado.setText(mViajeDetalle.isFinalizado() ?
                getResources().getText(R.string.viaje_estado_finalizado) :
                getResources().getText(R.string.viaje_estado_pendiente));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar_detalleviaje, menu);

        // Ajustar acciones según permisos del usuario conectado
        Usuario usuarioConectdo =
                ((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado;
        Rol rolUsuarioConectado = usuarioConectdo.getRol();

        // Si el usuario es un TCA O ADMIN habilitar la edición del viaje
        if (!rolUsuarioConectado.mayorIgual(Rol.TCA))
            menu.removeItem(R.id.acc_editar_viaje);
        // Si el usuario es un ADMIN habilitar el borrado del viaje
        if (!rolUsuarioConectado.mayorIgual(Rol.ADMIN))
            menu.removeItem(R.id.acc_borrar_viaje);
        /*
         * Si el usuario es un TCA o el conductor asignado al viaje y el viaje aún no está finalizado, habilitar la
         * creación de un informe de finalización
         */
        if (!puedeCrearInforme(usuarioConectdo)) {
            menu.removeItem(R.id.acc_crear_informe_finalizacion_viaje);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        MainActivity mainActivity = (MainActivity) getActivity();

        Bundle args = new Bundle();
        switch (item.getItemId()) {
            case R.id.acc_crear_informe_finalizacion_viaje: // Crear informe de finalización de viaje
                args.putSerializable(InformeViajeFragment.ARG_VIAJE_A_INFORMAR, mViajeDetalle);
                mainActivity.runOnUiThread(() ->
                        mainActivity.getNavController()
                                .navigate(R.id.navegacion_detalleviaje_a_informeviaje, args));
                break;
            case R.id.acc_editar_viaje: // Editar este viaje
                args.putSerializable(EditarViajeFragment.ARG_VIAJE_EDITADO, mViajeDetalle);
                mainActivity.runOnUiThread(() ->
                        mainActivity.getNavController()
                                .navigate(R.id.navegacion_detalleviaje_a_editarviaje, args));
                break;
            case R.id.acc_borrar_viaje: // Borrar este viaje
                mBorrarDialog.show(getParentFragmentManager(), "dialogoConfirmacionBorrarViaje");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Devuelve true si el usuario conectado puede crear un informe de finalización de viaje (si el viaje no está
     * finalizado y está asignado a él mismo o es TCA o superior)
     *
     * @param usuarioConectado El usuario conectado actualmente
     * @return True si el usuario actual puede crear un informe de finalización del viaje
     */
    private boolean puedeCrearInforme(Usuario usuarioConectado) {
        if (mViajeDetalle.isFinalizado()) {
            return false;
        } else if (usuarioConectado.getRol().mayorIgual(Rol.TCA)) {
            return true;
        } else {
            Long conductorAsignado = mViajeDetalle.getIdConductor();
            if (conductorAsignado == null) {
                return false;
            } else {
                return conductorAsignado.equals(usuarioConectado.getId());
            }
        }
    }
}