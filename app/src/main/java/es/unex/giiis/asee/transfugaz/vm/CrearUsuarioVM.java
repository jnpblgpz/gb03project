package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.UsuarioRepository;
import es.unex.giiis.asee.transfugaz.model.Usuario;

public class CrearUsuarioVM extends ViewModel {
	private UsuarioRepository repositorio;

	public CrearUsuarioVM(UsuarioRepository repositorio) {
		this.repositorio = repositorio;
	}

	public void insertarUsuario(Usuario usuario) {
		AppExecutors.getInstance()
			.diskIO()
			.execute(() -> {
				repositorio.insertar(usuario);
			});
	}
}
