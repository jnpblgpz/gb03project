package es.unex.giiis.asee.transfugaz.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.Objects;

import es.unex.giiis.asee.transfugaz.roomdb.RolConverter;

@Entity
public class Usuario implements Serializable {
	@PrimaryKey(autoGenerate = true)
	private Long id;
	private String nombre;
	private String contraseña;
	@TypeConverters(RolConverter.class)
	private Rol rol;

	public Usuario(Long id, String nombre, String contraseña, Rol rol) {
		this.id = id;
		this.nombre = nombre;
		this.contraseña = contraseña;
		this.rol = rol;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Usuario usuario = (Usuario) o;
		return Objects.equals(id, usuario.id) &&
			Objects.equals(nombre, usuario.nombre) &&
			Objects.equals(contraseña, usuario.contraseña) &&
			rol == usuario.rol;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nombre, contraseña, rol);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
}
