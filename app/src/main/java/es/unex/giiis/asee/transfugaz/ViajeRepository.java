package es.unex.giiis.asee.transfugaz;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.model.Viaje;
import es.unex.giiis.asee.transfugaz.roomdb.RolConverter;
import es.unex.giiis.asee.transfugaz.roomdb.UsuarioDAO;
import es.unex.giiis.asee.transfugaz.roomdb.ViajeDAO;

/**
 * Patrón Repository para manipular datos de usuarios
 */
public class ViajeRepository {
	private static ViajeRepository instancia;
	private final ViajeDAO viajeDAO;

	private ViajeRepository(ViajeDAO DAO) {
		viajeDAO = DAO;
	}

	public synchronized static ViajeRepository getInstance(ViajeDAO DAO) {
		if (instancia == null) {
			instancia = new ViajeRepository(DAO);
		}
		return instancia;
	}

	/**
	 * @return Un LiveData para observar los cambios sobre una colección de viajes leídos de la BD
	 */
	public LiveData<List<Viaje>> getViajes() {
		return viajeDAO.getTodos();
	}

	public LiveData<List<Viaje>> getPorConductor(Long idConductor) {
		return viajeDAO.getPorConductor(idConductor);
	}

	public LiveData<Viaje> getPorId(String id) {
		return viajeDAO.getPorId(id);
	}

	public long insertar(Viaje viaje) {
		return viajeDAO.insertar(viaje);
	}

	public int actualizar(Viaje viaje) {
		return viajeDAO.actualizar(viaje);
	}

	public void setFinalizado(Viaje viaje, boolean finalizado) {
		viaje.setFinalizado(finalizado);
		viajeDAO.actualizar(viaje);
	}

	public void borrar(String id) {
		viajeDAO.borrar(id);
	}
}
