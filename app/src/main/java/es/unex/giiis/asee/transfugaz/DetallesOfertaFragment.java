package es.unex.giiis.asee.transfugaz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;
import es.unex.giiis.asee.transfugaz.vm.DetallesOfertaFragmentViewModel;

public class DetallesOfertaFragment extends Fragment {

    public static final String ARG_ID_VIAJE_DETALLE = "idOferta";

    private final SimpleDateFormat dateExtendedFormat =
            new SimpleDateFormat("EEEE dd MMMM yyyy, HH:mm", Locale.getDefault());

    private TextView mTvCliente;
    private TextView mTvFechaCreacion;
    private TextView mTvOrigen;
    private TextView mTvDestino;
    private TextView mTvFechaEntrega;
    private TextView mTvRemuneracion;
    private TextView mTvEstado;
    private Button mBtnCalcularViabilidad;

    public DetallesOfertaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalles_oferta, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // RECUPERAR EL ID DE LA OFERTA DE LA QUE SE MUESTRAN SUS DETALLES
        long idOferta = (long) getArguments().getSerializable(ARG_ID_VIAJE_DETALLE);

        // OBTENER REFERENCIAS A LAS VISTAS

        mTvCliente = view.findViewById(R.id.tv_oferta_detalle_cliente);
        mTvFechaCreacion = view.findViewById(R.id.tv_oferta_detalle_fechacreacion);
        mTvOrigen = view.findViewById(R.id.tv_oferta_detalle_origen);
        mTvDestino = view.findViewById(R.id.tv_oferta_detalle_destino);
        mTvFechaEntrega = view.findViewById(R.id.tv_oferta_detalle_fechaentrega);
        mTvRemuneracion = view.findViewById(R.id.tv_oferta_detalle_remuneracion);
        mTvEstado = view.findViewById(R.id.tv_oferta_detalle_estado);
        mBtnCalcularViabilidad = view.findViewById(R.id.btn_oferta_calcularviabilildad);

        // CONFIGURACIÓN ADICIONAL DE LAS VISTAS QUE LO REQUIERAN

        AppTransFugaz.AppContainer contenedorDep = ((AppTransFugaz) getActivity().getApplication()).contenedorDep;
        DetallesOfertaFragmentViewModel viewModel =
                new ViewModelProvider(this, contenedorDep.appVMFactory)
                        .get(DetallesOfertaFragmentViewModel.class);

        // Especificar el identificador de la oferta de la que se muestra el detalle
        viewModel.setIdentificadorOferta(idOferta);

        // Observar la oferta de viaje y su estado
        viewModel.getOfertaViaje()
                .observe(getViewLifecycleOwner(), this::alCargarOfertaViaje);
        viewModel.getOfertaViajeBD()
                .observe(getViewLifecycleOwner(), this::alCargarOfertaViajeBD);

    }

    private void alCargarOfertaViaje(OfertaViaje ofertaViaje) {

        // POPULAR LAS VISTAS CON LOS DETALLES DE LA OFERTA

        mTvCliente.setText(ofertaViaje.getCliente());
        mTvFechaCreacion.setText(dateExtendedFormat.format(
                new Date(ofertaViaje.getFechaCreacion() * 1000)));
        mTvOrigen.setText(ofertaViaje.getOrigen());
        mTvDestino.setText(ofertaViaje.getDestino());
        mTvFechaEntrega.setText(dateExtendedFormat.format(
                new Date(ofertaViaje.getFechaEntrega() * 1000)));
        mTvRemuneracion.setText(getString(R.string.x_euros, ofertaViaje.getRemuneracion()));

        mBtnCalcularViabilidad.setOnClickListener(v -> {
            NavController navController = ((MainActivity) getActivity()).getNavController();
            Bundle args = new Bundle();
            args.putSerializable(ViabilidadFragment.NAV_ARG_ID_OFERTA, ofertaViaje.getId());
            navController.navigate(R.id.navegacion_paginacionOfertas_a_viabilidad, args);
        });
    }

    private void alCargarOfertaViajeBD(OfertaViaje.OfertaViajeDB ofertaViajeBD) {

        // Popular el estado de la oferta en la vista
        mTvEstado.setText(ofertaViajeBD.getEstado().toString());

        // Enlace a cálculo de viabilidad si el usuario tiene rol TCA o superior
        Usuario usuarioConectado =
                ((AppTransFugaz) getActivity().getApplication()).contenedorDep.usuarioConectado;
        if (usuarioConectado != null && usuarioConectado.getRol().mayorIgual(Rol.TCA)) {
            /*
             * El usuario puede calcular la viabilidad de ofertas, pero si la actual no tiene estado 'Pendiente', no se
             * debe mostrar el botón
             */
            if (ofertaViajeBD.getEstado() == EstadoOferta.PENDIENTE) {
                mBtnCalcularViabilidad.setVisibility(View.VISIBLE);
            } else {
                mBtnCalcularViabilidad.setVisibility(View.GONE);
            }
        } else {
            mBtnCalcularViabilidad.setVisibility(View.GONE);
        }
    }

}