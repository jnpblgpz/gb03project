package es.unex.giiis.asee.transfugaz.vm;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.transfugaz.AppExecutors;
import es.unex.giiis.asee.transfugaz.ViajeRepository;
import es.unex.giiis.asee.transfugaz.model.Viaje;

public class EditarViajeVM extends ViewModel {
	private ViajeRepository repositorio;

	public EditarViajeVM(ViajeRepository repositorio) {
		this.repositorio = repositorio;
	}

	public void editarViaje(Viaje viaje) {
		repositorio.actualizar(viaje);
	}
}
