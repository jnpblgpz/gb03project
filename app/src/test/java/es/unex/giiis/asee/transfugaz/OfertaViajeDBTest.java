package es.unex.giiis.asee.transfugaz;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.EstadoOferta;
import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

import static org.junit.Assert.assertEquals;

public class OfertaViajeDBTest {
	@Test
	public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje.OfertaViajeDB oferta = new OfertaViaje.OfertaViajeDB(0, EstadoOferta.PENDIENTE);
		long valor = 1234;

		Field f = oferta.getClass().getDeclaredField("id");
		f.setAccessible(true);
		f.setLong(oferta, valor);

		assertEquals(valor, oferta.getId());
	}

	@Test
	public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje.OfertaViajeDB oferta = new OfertaViaje.OfertaViajeDB(0, EstadoOferta.PENDIENTE);
		long valor = 1234;

		oferta.setId(valor);
		Field f = oferta.getClass().getDeclaredField("id");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getEstadoTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje.OfertaViajeDB oferta = new OfertaViaje.OfertaViajeDB(0, EstadoOferta.PENDIENTE);
		EstadoOferta valor = EstadoOferta.ACEPTADA;

		Field f = oferta.getClass().getDeclaredField("estado");
		f.setAccessible(true);
		f.set(oferta, valor);

		assertEquals(valor, oferta.getEstado());
	}

	@Test
	public void setEstadoTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje.OfertaViajeDB oferta = new OfertaViaje.OfertaViajeDB(0, EstadoOferta.PENDIENTE);
		EstadoOferta valor = EstadoOferta.ACEPTADA;

		oferta.setEstado(valor);
		Field f = oferta.getClass().getDeclaredField("estado");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}
}