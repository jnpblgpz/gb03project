package es.unex.giiis.asee.transfugaz;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.Rol;
import es.unex.giiis.asee.transfugaz.model.Usuario;

import static org.junit.Assert.assertEquals;

public class UsuarioTest {
	@Test
	public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		Long valor = 1234L;

		Field f = usuario.getClass().getDeclaredField("id");
		f.setAccessible(true);
		f.set(usuario, valor);

		assertEquals(valor, usuario.getId());
	}

	@Test
	public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		Long valor = 1234L;

		usuario.setId(valor);
		Field f = usuario.getClass().getDeclaredField("id");
		f.setAccessible(true);

		assertEquals(valor, f.get(usuario));
	}

	@Test
	public void getNombreTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		String valor = "usuario1";

		Field f = usuario.getClass().getDeclaredField("nombre");
		f.setAccessible(true);
		f.set(usuario, valor);

		assertEquals(valor, usuario.getNombre());
	}

	@Test
	public void setNombreTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		String valor = "usuario1";

		usuario.setNombre(valor);
		Field f = usuario.getClass().getDeclaredField("nombre");
		f.setAccessible(true);

		assertEquals(valor, f.get(usuario));
	}

	@Test
	public void getContraseñaTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		String valor = "pass";

		Field f = usuario.getClass().getDeclaredField("contraseña");
		f.setAccessible(true);
		f.set(usuario, valor);

		assertEquals(valor, usuario.getContraseña());
	}

	@Test
	public void setContraseñaTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		String valor = "pass";

		usuario.setContraseña(valor);
		Field f = usuario.getClass().getDeclaredField("contraseña");
		f.setAccessible(true);

		assertEquals(valor, f.get(usuario));
	}

	@Test
	public void getRolTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		Rol valor = Rol.ADMIN;

		Field f = usuario.getClass().getDeclaredField("rol");
		f.setAccessible(true);
		f.set(usuario, valor);

		assertEquals(valor, usuario.getRol());
	}

	@Test
	public void setRolTest() throws NoSuchFieldException, IllegalAccessException {
		Usuario usuario = new Usuario(0L, "", "", Rol.CONDUCTOR);
		Rol valor = Rol.ADMIN;

		usuario.setRol(valor);
		Field f = usuario.getClass().getDeclaredField("rol");
		f.setAccessible(true);

		assertEquals(valor, f.get(usuario));
	}
}