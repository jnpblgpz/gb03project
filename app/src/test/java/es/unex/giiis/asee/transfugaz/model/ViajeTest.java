package es.unex.giiis.asee.transfugaz.model;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test de integración del modelo {@link Viaje}
 */
public class ViajeTest {

    @Test
    public void getId_Should_ReturnReflectedId() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        String providedId = "ABC";
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, providedId);

        // Comprobar comportamiento esperado

        assertEquals(providedId, instance.getId());
    }

    @Test
    public void given_providedId_then_expectProvidedId() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        String expectedId = "ABC";
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setId(expectedId);

        Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals(expectedId, field.get(instance));
    }

    @Test
    public void getIdConductor_Should_ReturnReflectedIdConductor() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        Long providedIdConductor = 1L;
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("idConductor");
        field.setAccessible(true);
        field.set(instance, providedIdConductor);

        // Comprobar comportamiento esperado

        assertEquals(providedIdConductor, instance.getIdConductor());
    }

    @Test
    public void given_providedIdConductor_then_expectProvidedIdConductor() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        Long expectedIdConductor = 1L;
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setIdConductor(expectedIdConductor);

        Field field = instance.getClass().getDeclaredField("idConductor");
        field.setAccessible(true);
        assertEquals(expectedIdConductor, field.get(instance));
    }

    @Test
    public void getOrigen_Should_ReturnReflectedOrigen() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        String providedOrigen = "Origen";
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("origen");
        field.setAccessible(true);
        field.set(instance, providedOrigen);

        // Comprobar comportamiento esperado

        assertEquals(providedOrigen, instance.getOrigen());
    }

    @Test
    public void given_providedOrigen_then_expectProvidedOrigen() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        String expectedOrigen = "Origen";
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setOrigen(expectedOrigen);

        Field field = instance.getClass().getDeclaredField("origen");
        field.setAccessible(true);
        assertEquals(expectedOrigen, field.get(instance));
    }

    @Test
    public void getDestino_Should_ReturnReflectedDestino() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        String providedDestino = "Destino";
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("destino");
        field.setAccessible(true);
        field.set(instance, providedDestino);

        // Comprobar comportamiento esperado

        assertEquals(providedDestino, instance.getDestino());
    }

    @Test
    public void given_providedDestino_then_expectProvidedDestino() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        String expectedDestino = "Destino1";
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setDestino(expectedDestino);

        Field field = instance.getClass().getDeclaredField("destino");
        field.setAccessible(true);
        assertEquals(expectedDestino, field.get(instance));
    }

    @Test
    public void getFechaDescarga_Should_ReturnReflectedFechaDescarga() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        Calendar providedFechaDescarga = Calendar.getInstance();
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("fechaDescarga");
        field.setAccessible(true);
        field.set(instance, providedFechaDescarga);

        // Comprobar comportamiento esperado

        assertEquals(providedFechaDescarga, instance.getFechaDescarga());
    }

    @Test
    public void given_providedFechaDescarga_then_expectProvidedFechaDescarga() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        Calendar expectedFechaDescarga = Calendar.getInstance();
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setFechaDescarga(expectedFechaDescarga);

        Field field = instance.getClass().getDeclaredField("fechaDescarga");
        field.setAccessible(true);
        assertEquals(expectedFechaDescarga, field.get(instance));
    }

    @Test
    public void getMercancia_Should_ReturnReflectedMercancia() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        List<String> providedMercancia = Arrays.asList("Item 1", "Item 2", "Item 3");
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("mercancia");
        field.setAccessible(true);
        field.set(instance, providedMercancia);

        // Comprobar comportamiento esperado

        assertNotNull(instance.getMercancia());
        assertEquals(providedMercancia.size(), instance.getMercancia().size());
        assertEquals(providedMercancia, instance.getMercancia());
    }

    @Test
    public void given_providedMercancia_then_expectProvidedMercancia() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        List<String> expectedMercancia = Arrays.asList("Item 1", "Item 2", "Item 3");
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setMercancia(expectedMercancia);

        Field field = instance.getClass().getDeclaredField("mercancia");
        field.setAccessible(true);
        assertNotNull(field.get(instance));
        assertEquals(expectedMercancia.size(), ((List<String>) field.get(instance)).size());
        assertEquals(expectedMercancia, field.get(instance));
    }

    @Test
    public void getDistancia_Should_ReturnReflectedDistancia() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        float providedDistancia = 1f;
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("distancia");
        field.setAccessible(true);
        field.set(instance, providedDistancia);

        // Comprobar comportamiento esperado

        assertEquals(providedDistancia, instance.getDistancia(), 0.001f);
    }

    @Test
    public void given_providedDistancia_then_expectProvidedDistancia() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        float expectedDistancia = 1f;
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setDistancia(expectedDistancia);

        Field field = instance.getClass().getDeclaredField("distancia");
        field.setAccessible(true);
        assertEquals(expectedDistancia, field.get(instance));
    }

    @Test
    public void isFinalizado_Should_ReturnReflectedFinalizado() throws NoSuchFieldException, IllegalAccessException {

        // Valor proporcionado
        boolean providedFinalizado = true;
        // Instancia de prueba
        Viaje instance = new Viaje();

        Field field = instance.getClass().getDeclaredField("finalizado");
        field.setAccessible(true);
        field.set(instance, providedFinalizado);

        // Comprobar comportamiento esperado

        assertEquals(providedFinalizado, instance.isFinalizado());
    }

    @Test
    public void given_providedFinalizado_then_expectProvidedFinalizado() throws NoSuchFieldException, IllegalAccessException {

        // Valor esperado
        boolean expectedFinalizado = true;
        // Instancia de prueba
        Viaje instance = new Viaje();

        // Comprobar comportamiento esperado

        instance.setFinalizado(expectedFinalizado);

        Field field = instance.getClass().getDeclaredField("finalizado");
        field.setAccessible(true);
        assertEquals(expectedFinalizado, field.get(instance));
    }

}