package es.unex.giiis.asee.transfugaz;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;

import static org.junit.Assert.*;

public class OfertaViajeTest {
	@Test
	public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		Field f = oferta.getClass().getDeclaredField("id");
		f.setAccessible(true);
		f.setLong(oferta, valor);

		assertEquals(valor, oferta.getId());
	}

	@Test
	public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		oferta.setId(valor);
		Field f = oferta.getClass().getDeclaredField("id");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getClienteTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "cliente1";

		Field f = oferta.getClass().getDeclaredField("cliente");
		f.setAccessible(true);
		f.set(oferta, valor);

		assertEquals(valor, oferta.getCliente());
	}

	@Test
	public void setClienteTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "cliente1";

		oferta.setCliente(valor);
		Field f = oferta.getClass().getDeclaredField("cliente");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getFechaCreacionTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		Field f = oferta.getClass().getDeclaredField("fechaCreacion");
		f.setAccessible(true);
		f.setLong(oferta, valor);

		assertEquals(valor, oferta.getFechaCreacion());
	}

	@Test
	public void setFechaCreacionTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		oferta.setFechaCreacion(valor);
		Field f = oferta.getClass().getDeclaredField("fechaCreacion");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getOrigenTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "ciudad1";

		Field f = oferta.getClass().getDeclaredField("origen");
		f.setAccessible(true);
		f.set(oferta, valor);

		assertEquals(valor, oferta.getOrigen());
	}

	@Test
	public void setOrigenTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "ciudad1";

		oferta.setOrigen(valor);
		Field f = oferta.getClass().getDeclaredField("origen");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getFechaEntregaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		Field f = oferta.getClass().getDeclaredField("fechaEntrega");
		f.setAccessible(true);
		f.setLong(oferta, valor);

		assertEquals(valor, oferta.getFechaEntrega());
	}

	@Test
	public void setFechaEntregaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		long valor = 1234;

		oferta.setFechaEntrega(valor);
		Field f = oferta.getClass().getDeclaredField("fechaEntrega");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getDestinoTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "ciudad1";

		Field f = oferta.getClass().getDeclaredField("destino");
		f.setAccessible(true);
		f.set(oferta, valor);

		assertEquals(valor, oferta.getDestino());
	}

	@Test
	public void setDestinoTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		String valor = "ciudad1";

		oferta.setDestino(valor);
		Field f = oferta.getClass().getDeclaredField("destino");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getMercanciaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		List<String> valor = new ArrayList<String>();
		valor.add("carga1");
		valor.add("carga2");
		valor.add("carga3");

		Field f = oferta.getClass().getDeclaredField("mercancia");
		f.setAccessible(true);
		f.set(oferta, valor);

		assertEquals(valor, oferta.getMercancia());
	}

	@Test
	public void setMercanciaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		List<String> valor = new ArrayList<String>();
		valor.add("carga1");
		valor.add("carga2");
		valor.add("carga3");

		oferta.setMercancia(valor);
		Field f = oferta.getClass().getDeclaredField("mercancia");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getDistanciaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		float valor = 1234;

		Field f = oferta.getClass().getDeclaredField("distancia");
		f.setAccessible(true);
		f.setFloat(oferta, valor);

		assertEquals(valor, oferta.getDistancia(), 0.00001f);
	}

	@Test
	public void setDistanciaTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		float valor = 1234;

		oferta.setDistancia(valor);
		Field f = oferta.getClass().getDeclaredField("distancia");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}

	@Test
	public void getRemuneracionTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		float valor = 1234;

		Field f = oferta.getClass().getDeclaredField("remuneracion");
		f.setAccessible(true);
		f.setFloat(oferta, valor);

		assertEquals(valor, oferta.getRemuneracion(), 0.00001f);
	}

	@Test
	public void setRemuneracionTest() throws NoSuchFieldException, IllegalAccessException {
		OfertaViaje oferta = new OfertaViaje();
		float valor = 1234;

		oferta.setRemuneracion(valor);
		Field f = oferta.getClass().getDeclaredField("remuneracion");
		f.setAccessible(true);

		assertEquals(valor, f.get(oferta));
	}
}