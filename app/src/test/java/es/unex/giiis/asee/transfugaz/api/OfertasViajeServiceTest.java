package es.unex.giiis.asee.transfugaz.api;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import es.unex.giiis.asee.transfugaz.model.OfertaViaje;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test unitarios para el servicio {@link OfertasViajeService}
 */
public class OfertasViajeServiceTest {

    // Servidor web mockeado para que Retrofit lo consulte
    private MockWebServer mockServer;
    // Cada oferta de viaje que se debe recuperar del servidor web mockeado
    private final OfertaViaje mockOfertaViaje1 = new OfertaViaje(1, "Logística de Extremadura SL",
            1602496800,
            "Sauce 20, 28942 Fuenlabrada, Madrid",
            1602579600,
            "Can Fenosa 12, 08107 Barcelona",
            Arrays.asList("Gigabyte B450M DS3H", "Zotac GAMING GeForce RTX2060 6GB GDDR6"),
            506.685f,
            1120.0f);
    private final OfertaViaje mockOfertaViaje2 = new OfertaViaje(2, "Logística de Extremadura SL",
            1604838600,
            "Ana Mariscal, 10004 Cáceres",
            1605078000,
            "Rua Zapatería, 10600 Plasencia, Cáceres",
            Arrays.asList("Kingston A400 SSD 240GB", "MSI B450 Gaming Plus MAX"),
            67.93f,
            862.6f);
    private final OfertaViaje mockOfertaViaje3 = new OfertaViaje(3, "Fedex",
            1603202400,
            "Filiberto de Villalobos, 37007 Salamanca",
            1603450800,
            "Magallanes, 28903 Getafe, Madrid",
            Arrays.asList("Gigabyte GeForce RTX 2060 OC 6GB GDDR6", "Kioxia EXCERIA 480GB SSD SATA"),
            178.76f,
            1580.0f);
    // Lista de ofertas de viaje que se debe recuperar del servidor web mockeado
    private List<OfertaViaje> listaOfertasMockeadas = Arrays.asList(mockOfertaViaje1, mockOfertaViaje2, mockOfertaViaje3);

    @Before
    public void setUp() throws Exception {
        // Inicializar una nueva instancia del servidor web mock antes de cada test
        mockServer = new MockWebServer();
    }

    @Test
    public void shouldReturnMockedOffersFromRESTAPIService() throws IOException, InterruptedException {

        // Crear una respuesta mockeada con el objeto JSON que contiene la lista de ofertas de viaje
        MockResponse RESTMockRestponse = new MockResponse();
        RESTMockRestponse.setHeader("Content-Type", "application/JSON");
        RESTMockRestponse.setBody(
                "[{\"cliente\":\"Logística de Extremadura SL\",\"destino\":\"Can Fenosa 12, 08107 Barcelona\",\"distancia\":506.685,\"fechaCreacion\":1602496800,\"fechaEntrega\":1602579600,\"id\":1,\"mercancia\":[\"Gigabyte B450M DS3H\",\"Zotac GAMING GeForce RTX2060 6GB GDDR6\"],\"origen\":\"Sauce 20, 28942 Fuenlabrada, Madrid\",\"remuneracion\":1120.0}," +
                        "{\"cliente\":\"Logística de Extremadura SL\",\"destino\":\"Rua Zapatería, 10600 Plasencia, Cáceres\",\"distancia\":67.93,\"fechaCreacion\":1604838600,\"fechaEntrega\":1605078000,\"id\":2,\"mercancia\":[\"Kingston A400 SSD 240GB\",\"MSI B450 Gaming Plus MAX\"],\"origen\":\"Ana Mariscal, 10004 Cáceres\",\"remuneracion\":862.6}," +
                        "{\"cliente\":\"Fedex\",\"destino\":\"Magallanes, 28903 Getafe, Madrid\",\"distancia\":178.76,\"fechaCreacion\":1603202400,\"fechaEntrega\":1603450800,\"id\":3,\"mercancia\":[\"Gigabyte GeForce RTX 2060 OC 6GB GDDR6\",\"Kioxia EXCERIA 480GB SSD SATA\"],\"origen\":\"Filiberto de Villalobos, 37007 Salamanca\",\"remuneracion\":1580.0}]");
        // Añadir una respuesta con un cuerpo que describe una lista de ofertas en formato JSON
        mockServer.enqueue(RESTMockRestponse);

        // Iniciar el servidor web mock
        mockServer.start();

        // Inicializar Retrofit con el servidor web mock, dependencia por constructor del cargador de red
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Inicializar el servicio de red con Retrofit utilizando el servidor web mockeado

        OfertasViajeService servicioOfertas = retrofit.create(OfertasViajeService.class);
        Call<List<OfertaViaje>> call = servicioOfertas.listarOfertas();
        Response<List<OfertaViaje>> response = call.execute();

        // Comprobar el comportamiento

        assertNotNull(response.body());
        assertEquals(listaOfertasMockeadas.size(), response.body().size());
        assertEquals(listaOfertasMockeadas, response.body());
    }

    @After
    public void tearDown() throws Exception {
        // Liberar los recursos del servidor web mock
        mockServer.shutdown();
    }
}
